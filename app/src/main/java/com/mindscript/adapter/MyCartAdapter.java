package com.mindscript.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mindscript.categoryclick;
import com.mindscript.pojo.CartItemone;
import com.mindscript.shopyouneeds.R;
import com.mindscript.util.CartModel;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.ViewHolder> {


    Context context;
    ArrayList<CartItemone> list;
    int counter = 1;
    private static categoryclick categoryclick;

    public MyCartAdapter(Context context, ArrayList<CartItemone> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.custom_cart, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final CartItemone cartItem =list.get(position);
        // holder.imageView.setImageResource(cartItem.getP_photo());
        holder.tv_title.setText(cartItem.getP_name());
        holder.tv_kg.setText(cartItem.getP_quantity()+" "+cartItem.getUn_name());
        //holder.tv_price.setText("₹ "+cartItem.getP_price());
        String price=cartItem.getP_price();
        String discount=cartItem.getP_discount();
        String newPrice="";
        if (cartItem.getP_dis_type().equals("1")){
            holder.tv_offer.setVisibility(View.VISIBLE);
            Log.i("nik",cartItem.getP_discount());
            holder.tv_offer.setText("₹ "+cartItem.getP_discount()+" OFF");
            newPrice=String.valueOf((Integer.parseInt(price) -  Integer.parseInt(discount)));
            holder.tv_price.setText("₹ "+price);
           // holder.tv_price.setText(price);
        }else if (cartItem.getP_dis_type().equals("2")){
            holder.tv_offer.setVisibility(View.VISIBLE);
            holder.tv_offer.setText(cartItem.getP_discount()+"% OFF");
            newPrice=String.valueOf((Integer.parseInt(price)- ((Integer.parseInt(price) * Integer.parseInt(discount)) / 100)));
            holder.tv_price.setText("₹ "+price);
            //holder.tv_price.setText(price);
        }else {
            holder.tv_offer.setVisibility(View.GONE);
            holder.tv_price.setText("₹ "+price);
//            holder.tv_price.setText(price);
        }

        // holder.tv_quantity.setText("Quantity: "+cartItem.getU_quantity());
        Picasso.with(context).load(cartItem.getP_photo()).into(holder.imageView);
        CartModel.getInstance(context);
        final String u_id= SharedPreference.get("u_id");
        CartModel.open();
        // String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + cartItem.getId() + "'";
        String sql="select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + cartItem.getId() + "'";
        Loggers.i(sql);
        Cursor cursor = CartModel.database.rawQuery(sql ,null);
        if(cursor != null){
            Loggers.i("======================= " + cursor.getCount());
            if(cursor.moveToFirst()){
                String u_quantity = cursor.getString(12);
                holder.tv_cartCount.setText(u_quantity);
                counter=Integer.parseInt(u_quantity);
            }

        }else{
            counter=1;
            holder.tv_cartCount.setText("1");

        }


        /*while(cursor.moveToNext()) {
           String p_id = cursor.getString(2);

            if (p_id.equals(productList.getP_id())){
                Loggers.i("P_ID => " + p_id);

            }else {

            }

        }*/
        CartModel.close();


        holder.ivadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                increaseInteger(view, holder);
                CartModel.open();
                //  String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + cartItem.getId() + "'";
                String sql="select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + cartItem.getId() + "'";
                Cursor cursor = CartModel.database.rawQuery(sql,null);
                while(cursor.moveToNext()) {
                    String p_id = cursor.getString(2);
                    if (p_id.equals(cartItem.getId())){
                        String u_quantity = cursor.getString(12);
                        SQLiteDatabase db = CartModel.dbHelper.getReadableDatabase();
                        ContentValues contentValues = new ContentValues();
                        holder.tv_cartCount.setText(String.valueOf((Integer.parseInt(u_quantity)+ 1)));
                        counter=Integer.parseInt(String.valueOf((Integer.parseInt(u_quantity)+ 1)));
                        contentValues.put(Keys.CART.U_QUANTITY,String.valueOf((Integer.parseInt(u_quantity)+ 1)));
                        db.update(Keys.CARTTABLE.TB_NAME, contentValues, "p_id = ?", new String[]{p_id});
                        Log.i("nik","value increment updated");
                        CartModel.close();
                    }else{

                    }

                }


            }
        });
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CartModel.open();
                //String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + cartItem.getId() + "'";
                String sql="select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + cartItem.getId() + "'";
                Cursor cursor = CartModel.database.rawQuery(sql,null);
                while(cursor.moveToNext()) {
                    String p_id = cursor.getString(2);
                    if (p_id.equals(cartItem.getId())) {
                        String u_quantity = cursor.getString(12);
                        SQLiteDatabase db = CartModel.dbHelper.getWritableDatabase();
                        ContentValues contentValues = new ContentValues();
                        holder.tv_cartCount.setText(u_quantity);
                        counter=Integer.parseInt(u_quantity);
                        if(counter > 1){
                            contentValues.put(Keys.CART.U_QUANTITY, String.valueOf((Integer.parseInt(u_quantity) - 1)));
                            db.update(Keys.CARTTABLE.TB_NAME, contentValues, "p_id = ? ", new String[]{p_id});
                            Log.i("nik", "value increment updated");
                            CartModel.close();
                        }else{
                            db.delete(Keys.CARTTABLE.TB_NAME, "p_id = ? ", new String[]{cartItem.getId()});
                            Log.i("nik","value decrement updated");
                            CartModel.close();

                        }
                        counter-=1;
                        if(counter == 0){
                            notifyDataSetChanged();
                            if (categoryclick!=null){
                                categoryclick.click("9");
                            }
//                            holder.cd.setVisibility(View.GONE);
//                            holder.button_buy.setVisibility(View.VISIBLE);
//                            holder.llCartAddRemove.setVisibility(View.GONE);
                        }else {
                            holder.tv_cartCount.setText(String.valueOf(counter));
                        }
                    } else {

                    }


                    //decreaseInteger(view, holder);
                }



            }
        });


    }
    private void increaseInteger(View view, ViewHolder holder) {
//        if (counter == 5) {
//
//        } else {
        if (counter > -1) {
            counter = counter + 1;
            display(counter, holder,"increase");
        }
        // }
    }
    private void display(int number, ViewHolder holder, String type) {
        holder.tv_cartCount.setText("" + number);
        String cartItem=holder.tv_cartCount.getText().toString();
        if (type.equals("decrease")){



        }

//        Intent intent = new Intent("custom-message");
//        intent.putExtra("cartItem",cartItem);
//        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tv_title,tv_kg,tv_price,tv_quantity,tv_offer;
        CardView cd;
        ImageView ivRemove, ivadd;
        TextView tv_cartCount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.p_image);
            tv_title=itemView.findViewById(R.id.p_name);
            tv_kg=itemView.findViewById(R.id.p_kg);
            tv_price=itemView.findViewById(R.id.p_price);
            ivRemove = itemView.findViewById(R.id.imageViewCartremovefromcart);
            ivadd = itemView.findViewById(R.id.imageViewCartaddfromcart);
            tv_cartCount = itemView.findViewById(R.id.textViewCartCartCount);
            cd=itemView.findViewById(R.id.cardViewCart);
            tv_offer=itemView.findViewById(R.id.txt_offer);
            // tv_quantity=itemView.findViewById(R.id.txt_quantity);



        }
    }
    public  static void bindListener(categoryclick Listener){
        categoryclick=Listener;
    }

}
