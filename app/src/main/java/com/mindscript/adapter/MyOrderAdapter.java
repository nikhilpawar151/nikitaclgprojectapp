package com.mindscript.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mindscript.pojo.OrderList;
import com.mindscript.shopyouneeds.OrderDetailsActivity;
import com.mindscript.shopyouneeds.R;


import java.util.ArrayList;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.ViewHolder> {
    Context context;
    ArrayList<OrderList> list;



    public MyOrderAdapter(Context context, ArrayList<OrderList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.custom_order, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final OrderList orderList =list.get(position);
        holder.tv_order_no.setText("Order " + orderList.getO_no());
        holder.tv_date.setText(orderList.getO_date());
        String o_status="";
        if (orderList.getO_status().equals("0")){
            o_status="Submitted";
            holder.tv_status.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context,R.drawable.ic_clock), null);
        }else if (orderList.getO_status().equals("1")){
            o_status="Accepted";
            holder.tv_status.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context,R.drawable.ic_check), null);
        }
        else if (orderList.getO_status().equals("2")){
            o_status="Rejected";
            holder.tv_status.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context,R.drawable.ic_cancel), null);
        } else if (orderList.getO_status().equals("3")){
            o_status="On The Way";
            holder.tv_status.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context,R.drawable.ic_delivery), null);
        } else if (orderList.getO_status().equals("4")){
            o_status="Delivered";
            holder.tv_status.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context,R.drawable.ic_delivery_truck), null);
        }

        holder.tv_status.setText(o_status);
        holder.tv_total.setText("₹"+ orderList.getO_total());

        holder.cd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("o_id", orderList.getO_id());
                intent.putExtra("o_no", orderList.getO_no());
                intent.putExtra("o_time", orderList.getO_date());
                intent.putExtra("o_total", orderList.getO_total());
                intent.putExtra("o_status", orderList.getO_status());
                intent.putExtra("type", "order");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_order_no,tv_date,tv_status,tv_total;
        CardView cd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_order_no=itemView.findViewById(R.id.o_no);
            tv_date=itemView.findViewById(R.id.o_date);
            tv_status=itemView.findViewById(R.id.o_status);
            tv_total=itemView.findViewById(R.id.o_total);
            cd=itemView.findViewById(R.id.order_cd);



        }
    }

}
