package com.mindscript.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.mindscript.categoryclick;
import com.mindscript.pojo.AllProduct;
import com.mindscript.pojo.ProductList;
import com.mindscript.shopyouneeds.MyCartActivity;
import com.mindscript.shopyouneeds.R;
import com.mindscript.util.CartModel;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AllProductAdapter extends RecyclerView.Adapter<AllProductAdapter.ViewHolder> implements Filterable {
    Context context;
    ArrayList<AllProduct> list;
    ArrayList<ProductList> productLists;
    ArrayList<AllProduct> allProducts=new ArrayList<>();
    int counter = 1;
    String quantity, id, path;
    private static categoryclick categoryclick;


    public AllProductAdapter(Context context, ArrayList<AllProduct> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.custom_product_list, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final AllProduct product = list.get(position);
        // holder.imageView.setImageResource(product.getP_photo());
        Picasso.with(context).load(Keys.PRODUCT_PATH + product.getP_photo()).into(holder.imageView);
        holder.tv_category.setText(product.getP_name());
        holder.tv_kg.setText(product.getP_quantity() + product.getUn_name());
        holder.tv_price.setText(product.getP_price());

        CartModel.getInstance(context);
        final String u_id = SharedPreference.get("u_id");
        quantity = holder.tv_cartCount.getText().toString();
        path = Keys.PRODUCT_PATH + product.getP_photo();
        CartModel.open();
        // String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id() + "'";
        String sql = "select * from " + Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + product.getP_id() + "'";
        Loggers.i(sql);
        Cursor cursor = CartModel.database.rawQuery(sql, null);
        if (cursor != null) {
            Loggers.i("======================= " + cursor.getCount());
            if (cursor.moveToFirst()) {
                String u_quantity = cursor.getString(10);
                holder.btnadd.setVisibility(View.GONE);
                holder.llCartAddRemove.setVisibility(View.VISIBLE);
                holder.tv_cartCount.setText(u_quantity);
                counter = Integer.parseInt(u_quantity);
            }else{
                counter = 1;
                holder.tv_cartCount.setText("1");
                holder.btnadd.setVisibility(View.VISIBLE);
                holder.llCartAddRemove.setVisibility(View.GONE);
            }

        } else {
            counter = 1;
            holder.tv_cartCount.setText("1");
            holder.btnadd.setVisibility(View.VISIBLE);
            holder.llCartAddRemove.setVisibility(View.GONE);
        }
        CartModel.close();

        holder.cd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(context,R.style.BottomSheetDialogTheme);
//                View bottomSheetview= LayoutInflater.from(context)
//                        .inflate(R.layout.bottom_sheet,(LinearLayout)view.findViewById(R.id.bottom_container));
//                bottomSheetDialog.setContentView(bottomSheetview);
//                bottomSheetDialog.show();
//                final TextView tv_category,tv_kg,tv_price,tv_desc;
//
//                final ImageView product_img;
//                tv_category=bottomSheetview.findViewById(R.id.txt_category);
//                tv_desc=bottomSheetview.findViewById(R.id.txt_description);
//
//
//                product_img=bottomSheetview.findViewById(R.id.category_img);
//                tv_category.setText(product.getP_name());
//                tv_desc.setText(product.getP_description());
//                Picasso.with(context).load(Keys.PRODUCT_PATH + product.getP_photo()).into(product_img);
//                product_img.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        AlertDialog.Builder builder=new AlertDialog.Builder(context,R.style.myFullscreenAlertDialogStyle);
//                        View bottomSheetview= LayoutInflater.from(context)
//                                .inflate(R.layout.custom_alert,(LinearLayout)view.findViewById(R.id.alert_container));
//                        builder.setView(bottomSheetview);
//                        final AlertDialog dialog = builder.create();
//                        dialog.show();
//                        PhotoView photoView;
//                        ImageView imageView;
//                        imageView=bottomSheetview.findViewById(R.id.img_cancel);
//                        imageView.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                dialog.dismiss();
//                            }
//                        });
//                        photoView=(PhotoView)bottomSheetview.findViewById(R.id.categoryImg);
//                        Picasso.with(context).load(Keys.PRODUCT_PATH + product.getP_photo()).into(photoView);
//                    }
//                });


//                tv_kg=bottomSheetview.findViewById(R.id.txt_kg);
//                tv_price=bottomSheetview.findViewById(R.id.txt_price);

            }
        });

        holder.btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSnack(view);
                CartModel.open();
                CartModel.insert("0", product.getP_id(), product.getC_id(), product.getP_name(), product.getP_price(),
                        product.getP_quantity(), product.getUn_id(), Keys.PRODUCT_PATH + product.getP_photo(), product.getUn_name(), "1",product.getP_dis_type(),product.getP_discount());
                CartModel.close();
                holder.btnadd.setVisibility(View.GONE);
                holder.llCartAddRemove.setVisibility(View.VISIBLE);
                if (categoryclick != null) {
                    categoryclick.click(product.getC_id());
                }

            }
        });
        holder.ivadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSnack(view);
                increaseInteger(view, holder);
                CartModel.open();
                // String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id() + "'";
                String sql = "select * from " + Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + product.getP_id() + "'";
                Cursor cursor = CartModel.database.rawQuery(sql, null);
                while (cursor.moveToNext()) {
                    String p_id = cursor.getString(2);
                    if (p_id.equals(product.getP_id())) {
                        String u_quantity = cursor.getString(10);
                        SQLiteDatabase db = CartModel.dbHelper.getReadableDatabase();
                        ContentValues contentValues = new ContentValues();
                        holder.tv_cartCount.setText(String.valueOf((Integer.parseInt(u_quantity) + 1)));
                        counter = Integer.parseInt(String.valueOf((Integer.parseInt(u_quantity) + 1)));
                        contentValues.put(Keys.CART.U_QUANTITY, String.valueOf((Integer.parseInt(u_quantity) + 1)));
                        db.update(Keys.CARTTABLE.TB_NAME, contentValues, "p_id = ?", new String[]{p_id});
                        Log.i("nik", "value increment updated");
                        CartModel.close();
                    } else {

                    }

                }

            }
        });
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CartModel.open();
                // String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id() + "'";
                String sql = "select * from " + Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + product.getP_id() + "'";
                Cursor cursor = CartModel.database.rawQuery(sql, null);
                while (cursor.moveToNext()) {
                    String p_id = cursor.getString(2);
                    if (p_id.equals(product.getP_id())) {
                        String u_quantity = cursor.getString(10);
                        SQLiteDatabase db = CartModel.dbHelper.getWritableDatabase();
                        ContentValues contentValues = new ContentValues();
                        holder.tv_cartCount.setText(u_quantity);
                        counter = Integer.parseInt(u_quantity);
                        if (counter > 1) {
                            contentValues.put(Keys.CART.U_QUANTITY, String.valueOf((Integer.parseInt(u_quantity) - 1)));
                            db.update(Keys.CARTTABLE.TB_NAME, contentValues, "p_id = ? ", new String[]{p_id});
                            Log.i("nik", "value increment updated");
                            CartModel.close();
                        } else {
                            db.delete(Keys.CARTTABLE.TB_NAME, "p_id = ?", new String[]{product.getP_id()});
                            Log.i("nik", "value decrement updated");
                            CartModel.close();
                            if (categoryclick != null) {
                                categoryclick.click(product.getC_id());
                            }
                        }
                        counter -= 1;
                        if (counter == 0) {
                            holder.btnadd.setVisibility(View.VISIBLE);
                            holder.llCartAddRemove.setVisibility(View.GONE);
                        } else {
                            holder.tv_cartCount.setText(String.valueOf(counter));
                        }
                    } else {

                    }


                    //decreaseInteger(view, holder);
                }
            }
        });


    }

    private void increaseInteger(View view, ViewHolder holder) {
        if (counter > -1) {
            counter = counter + 1;
            display(counter,holder,"increase");
        }
    }

    private void display(int number, ViewHolder holder,String type) {
        holder.tv_cartCount.setText("" + number);
        String cartItem=holder.tv_cartCount.getText().toString();
        if (type.equals("decrease")){

        }
    }

    private void showSnack(View view) {
        Snackbar snackBar = Snackbar.make(view, "Item added to cart", Snackbar.LENGTH_LONG).setAction("View Cart", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyCartActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        snackBar.setActionTextColor(Color.WHITE);
        View snackBarView = snackBar.getView();
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackBar.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public  static void bindListener(com.mindscript.categoryclick Listener){
        categoryclick=Listener;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.i("nik",constraint+"");
                final FilterResults oReturn = new FilterResults();
                final ArrayList<AllProduct> results = new ArrayList<AllProduct>();
                if (allProducts == null)
                    allProducts = list;
                if (constraint != null) {
                    if (allProducts != null && allProducts.size() > 0) {

                        for (final AllProduct g : allProducts) {
                            // Log.i("nik",g.get.toString());
                            // ArrayList<String> keyword = new ArrayList<String>(Arrays.asList(g.getB_keywords().split(",")));
                            if (g.getP_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                results.add(g);
                                Log.i("nik",g+"");
                            }

                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                list = (ArrayList<AllProduct>) results.values;
                notifyDataSetChanged();
            }
        };
    }
//    public void increaseInteger(View view, ViewHolder holder) {
//        if (counter > -1) {
//            counter = counter + 1;
//            display(counter, holder, "increase");
//        }
//    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, ivRemove, ivadd;
        TextView tv_category, tv_kg, tv_price, tv_cartCount;
        CardView cd;
        LinearLayout llCartAddRemove;
        Button btnadd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.category_img);
            tv_category = itemView.findViewById(R.id.txt_category);
            tv_kg = itemView.findViewById(R.id.txt_kg);

            llCartAddRemove = itemView.findViewById(R.id.linearLayoutCartAddRemove);
            btnadd = itemView.findViewById(R.id.buttonPaddtoCart);
            ivRemove = itemView.findViewById(R.id.imageViewPremovefromcart);
            ivadd = itemView.findViewById(R.id.imageViewPaddfromcart);
            tv_cartCount = itemView.findViewById(R.id.textViewPCartCount);
            cd = itemView.findViewById(R.id.product_cd);


        }
    }

    public void decreaseInteger(View view, ViewHolder holder) {
        if (counter < 1) {

        } else {
            counter = counter - 1;
            //display(counter,holder);
        }
    }


}
