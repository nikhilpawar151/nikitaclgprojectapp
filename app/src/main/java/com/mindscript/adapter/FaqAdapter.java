package com.mindscript.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.mindscript.pojo.Faq;
import com.mindscript.shopyouneeds.R;

import java.util.ArrayList;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.FaqViewHolder> {
    ArrayList<Faq> alFaq=new ArrayList<>();
    Context context;
    boolean isopen=false;

    public FaqAdapter(ArrayList<Faq> alFaq, Context context) {
        this.alFaq = alFaq;
        this.context = context;
    }

    @NonNull
    @Override
    public FaqViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View itemView  = layoutInflater.inflate(R.layout.custom_faq, parent, false);
        itemView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new FaqViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FaqViewHolder holder, int position) {
        Faq faq=alFaq.get(position);
        holder.tvQues.setText(faq.getQues());
        holder.tvAns.setText(faq.getAns());

        holder.tvQues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isopen){
                    holder.tvQues.setBackgroundResource(R.drawable.faqoutline);
                    holder.tvQues.setTextColor(context.getResources().getColor(R.color.black));
                    holder.tvQues.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_up_24,0);
                    holder.tvAns.setVisibility(View.GONE);
                    isopen=false;
                }else {
                    holder.tvQues.setBackgroundResource(R.drawable.faqfilled);
                    holder.tvQues.setTextColor(context.getResources().getColor(R.color.white));
                    holder.tvQues.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.arrow_down,0);
                    holder.tvAns.setVisibility(View.VISIBLE);
                    isopen=true;
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return alFaq.size();
    }

    public class FaqViewHolder extends RecyclerView.ViewHolder {
        TextView tvQues,tvAns;
        public FaqViewHolder(@NonNull View itemView) {
            super(itemView);
            tvQues=itemView.findViewById(R.id.tvfaqques);
            tvAns=itemView.findViewById(R.id.tvfaqans);
        }
    }
}
