package com.mindscript.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mindscript.pojo.OrderDetails;
import com.mindscript.shopyouneeds.R;
import com.mindscript.util.Keys;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.ViewHolder> {
    Context context;
    ArrayList<OrderDetails> list;



    public OrderDetailsAdapter(Context context, ArrayList<OrderDetails> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.custom_order_detail, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final OrderDetails od =list.get(position);
        Picasso.with(context)
                .load(Keys.PRODUCT_PATH + od.getP_photo())
                .into(holder.imageView);
        holder.tv_title.setText(od.getP_name());
        holder.tv_kg.setText(od.getP_quantity()+" "+od.getUn_name()+" ( "+"₹ "+od.getP_rate()+" )");
        int total=Integer.parseInt(od.getP_rate()) * od.getP_quantity();
        holder.tv_price.setText(od.getP_quantity()+" = "+"₹ "+total);
        // +

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tv_title,tv_kg,tv_price;
        CardView cd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.p_image);
            tv_title=itemView.findViewById(R.id.p_name);
            tv_kg=itemView.findViewById(R.id.p_kg);
            tv_price=itemView.findViewById(R.id.p_price);



        }
    }
}
