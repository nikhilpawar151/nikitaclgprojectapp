package com.mindscript.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;
import com.mindscript.pojo.ProductList;
import com.mindscript.shopyouneeds.MyCartActivity;
import com.mindscript.shopyouneeds.ProductActivity;
import com.mindscript.shopyouneeds.R;
import com.mindscript.util.AppController;
import com.mindscript.util.CartModel;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> implements Filterable {

    Context context;
    ArrayList<ProductList> list;
    ArrayList<ProductList> suggestionList=new ArrayList<>();
    ArrayList<ProductList> orig;
    int counter = 1;
    int total=0;
    ProductList productList;
    String quantity,id,path;
    private static com.mindscript.categoryclick categoryclick;
    private static com.mindscript.productclick productclick;



    public ProductAdapter(Context context, ArrayList<ProductList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.custom_product, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ProductList productList = list.get(position);
        //   holder.imageView.setImageResource(productList.getPhoto());
        holder.tv_name.setText(productList.getP_name());
        holder.tv_kg.setText(productList.getP_quantity() + " "+productList.getUn_name());
        //holder.tv_new_price.setText("₹ " + productList.getP_price());
        String price = productList.getP_price();
        String newPrice="";
        holder.tv_old_price.setText("₹ "+price);
        if (productList.getP_dis_type().equals("1")){
            holder.tv_offer.setVisibility(View.VISIBLE);
            holder.tv_old_price.setVisibility(View.VISIBLE);
            holder.tv_offer.setText("₹ "+productList.getP_discount()+" OFF");
            newPrice=String.valueOf((Integer.parseInt(price)-  Integer.parseInt(productList.getP_discount())));
            holder.tv_new_price.setText("₹ "+newPrice);
        }else if (productList.getP_dis_type().equals("2")){
            holder.tv_offer.setVisibility(View.VISIBLE);
            holder.tv_offer.setText(productList.getP_discount()+"% OFF");
            holder.tv_old_price.setVisibility(View.VISIBLE);
            newPrice=String.valueOf((Integer.parseInt(price)- ((Integer.parseInt(price) * Integer.parseInt(productList.getP_discount())) / 100)));
            holder.tv_new_price.setText("₹ "+ newPrice);
        }else {
            holder.tv_offer.setVisibility(View.GONE);
            holder.tv_old_price.setVisibility(View.GONE);
            holder.tv_new_price.setText("₹ "+productList.getP_price());
        }
        if (productList.getP_enable().equals("1")){
            holder.tv_stock.setText("In Stock");
            holder.button_buy.setVisibility(View.VISIBLE);
            holder.tv_stock.setBackground(context.getResources().getDrawable(R.drawable.transgreencurve));
            holder.button_buy.setImageResource(R.drawable.ic_cart);
            holder.button_buy.setPadding(6,6,6,6);
            holder.button_buy.setBackground(context.getResources().getDrawable(R.drawable.btnbuybg));
            holder.button_buy.setColorFilter(context.getResources().getColor(R.color.white));
        }else if(productList.getP_enable().equals("0")){
            holder.tv_stock.setText("Out Of Stock");
            holder.tv_stock.setBackground(context.getResources().getDrawable(R.drawable.transredcurve));
            holder.button_buy.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            holder.button_buy.setVisibility(View.GONE);
        }
        holder.tv_old_price.setPaintFlags(holder.tv_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tv_name.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        holder.tv_kg.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        holder.tv_new_price.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        holder.tv_old_price.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        holder.imageView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        Picasso.with(context).load(Keys.PRODUCT_PATH + productList.getP_photo()).into(holder.imageView);
        holder.tv_offer.setBackground(context.getResources().getDrawable(R.drawable.offerbg));
        //holder.tv_offer.setText("10% Offf");

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params1.setMargins(15, -5, 0, 0);
        holder.tv_kg.setLayoutParams(params1);

        CartModel.getInstance(context);
        final String u_id= SharedPreference.get("u_id");
        quantity=holder.tv_cartCount.getText().toString();
        path= Keys.PRODUCT_PATH + productList.getP_photo();
        CartModel.open();
        // String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id() + "'";
        String sql="select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id()  + "'";
        Loggers.i(sql);
        Cursor cursor = CartModel.database.rawQuery(sql ,null);
        if(cursor != null){
            Loggers.i("======================= " + cursor.getCount());
            if(cursor.moveToFirst()){
                String u_quantity = cursor.getString(12);
                holder.button_buy.setVisibility(View.GONE);
                holder.llCartAddRemove.setVisibility(View.VISIBLE);
                holder.tv_cartCount.setText(u_quantity);
                counter=Integer.parseInt(u_quantity);
            }else{
                counter = 1;
                holder.tv_cartCount.setText("1");
                holder.button_buy.setVisibility(View.VISIBLE);
                holder.llCartAddRemove.setVisibility(View.GONE);
            }

        }else{
            counter=1;
            holder.tv_cartCount.setText("1");
            holder.button_buy.setVisibility(View.VISIBLE);
            holder.llCartAddRemove.setVisibility(View.GONE);
        }


        /*while(cursor.moveToNext()) {
           String p_id = cursor.getString(2);

            if (p_id.equals(productList.getP_id())){
                Loggers.i("P_ID => " + p_id);

            }else {

            }

        }*/
        CartModel.close();
        holder.button_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSnack(view);
                CartModel.open();
                CartModel.insert("0",productList.getP_id(),productList.getC_id(),productList.getP_name(),productList.getP_price(),
                        productList.getP_dis_type(),productList.getP_discount(), productList.getP_quantity(),productList.getUn_id(),Keys.PRODUCT_PATH + productList.getP_photo(),productList.getUn_name(),"1");

                Log.i("nik",productList.getP_discount());
                CartModel.close();
                holder.button_buy.setVisibility(View.GONE);
                holder.llCartAddRemove.setVisibility(View.VISIBLE);
                if (categoryclick!=null){
                    categoryclick.click(productList.getP_id());
                }
                String u_id=SharedPreference.get("u_id");
                suggestionProducts(u_id,productList.getP_id(),holder);
               // holder.recyclerView_suggestion.setVisibility(View.VISIBLE);


                if (productclick!=null){
                    categoryclick.click(productList.getP_id());
                }

            }
        });

        holder.ivadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSnack(view);
                increaseInteger(view, holder);
                CartModel.open();
                // String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id() + "'";
                String sql="select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id()  + "'";
                Cursor cursor = CartModel.database.rawQuery(sql,null);
                while(cursor.moveToNext()) {
                    String p_id = cursor.getString(2);
                    if (p_id.equals(productList.getP_id())){
                        String u_quantity = cursor.getString(12);
                        SQLiteDatabase db = CartModel.dbHelper.getReadableDatabase();
                        ContentValues contentValues = new ContentValues();
                        holder.tv_cartCount.setText(String.valueOf((Integer.parseInt(u_quantity)+ 1)));
                        counter=Integer.parseInt(String.valueOf((Integer.parseInt(u_quantity)+ 1)));
                        contentValues.put(Keys.CART.U_QUANTITY,String.valueOf((Integer.parseInt(u_quantity)+ 1)));
                        db.update(Keys.CARTTABLE.TB_NAME, contentValues, "p_id = ?", new String[]{p_id});
                        Log.i("nik","value increment updated");
                        CartModel.close();
                    }else{

                    }

                }


            }
        });
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CartModel.open();
                // String sql = "select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id() + "'";
                String sql="select * from "+ Keys.CARTTABLE.TB_NAME + " WHERE " + Keys.CART.P_ID + " like '" + productList.getP_id()  + "'";
                Cursor cursor = CartModel.database.rawQuery(sql,null);
                while(cursor.moveToNext()) {
                    String p_id = cursor.getString(2);
                    if (p_id.equals(productList.getP_id())) {
                        String u_quantity = cursor.getString(12);
                        SQLiteDatabase db = CartModel.dbHelper.getWritableDatabase();
                        ContentValues contentValues = new ContentValues();
                        holder.tv_cartCount.setText(u_quantity);
                        counter=Integer.parseInt(u_quantity);
                        if(counter > 1){
                            contentValues.put(Keys.CART.U_QUANTITY, String.valueOf((Integer.parseInt(u_quantity) - 1)));
                            db.update(Keys.CARTTABLE.TB_NAME, contentValues, "p_id = ? ", new String[]{p_id});
                            Log.i("nik", "value increment updated");
                            CartModel.close();
                        }else{
                            db.delete(Keys.CARTTABLE.TB_NAME, "p_id = ?", new String[]{productList.getP_id()});
                            Log.i("nik","value decrement updated");
                            CartModel.close();
                            if (categoryclick!=null){
                                categoryclick.click(productList.getC_id());
                            }
                        }
                        counter-=1;
                        if(counter == 0){
                            holder.button_buy.setVisibility(View.VISIBLE);
                            holder.llCartAddRemove.setVisibility(View.GONE);
                        }else {
                            holder.tv_cartCount.setText(String.valueOf(counter));
                        }
                    } else {

                    }


                    //decreaseInteger(view, holder);
                }



            }
        });
        holder.cd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                View bottomSheetview= LayoutInflater.from(context)
                        .inflate(R.layout.alert_sheet,(LinearLayout)view.findViewById(R.id.alert_container));
                alertDialogBuilder.setView(bottomSheetview);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                final TextView tv_product_name,tv_product_type,tv_desc,txt_desc;
                final ImageView img_product;
                Button button_ok;
                tv_product_name=bottomSheetview.findViewById(R.id.txt_product_name);
                tv_product_type=bottomSheetview.findViewById(R.id.txt_category);
                tv_desc=bottomSheetview.findViewById(R.id.txt_description);
                img_product=bottomSheetview.findViewById(R.id.product_img);
                button_ok=bottomSheetview.findViewById(R.id.btn_ok);
                txt_desc=bottomSheetview.findViewById(R.id.tv_desc);
                txt_desc.setText("Description");
                txt_desc.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                tv_product_name.setText(productList.getP_name());
                tv_product_type.setText(productList.getC_name());
                tv_desc.setText(productList.getP_description());
                img_product.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                tv_product_name.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                tv_product_type.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                tv_desc.setBackgroundColor(context.getResources().getColor(R.color.transparent));

                Picasso.with(context).load(Keys.PRODUCT_PATH + productList.getP_photo()).into(img_product);

                button_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

//                BottomSheetDialog bottomSheetDialog=new BottomSheetDialog(context,R.style.BottomSheetDialogTheme);
//                View bottomSheetview= LayoutInflater.from(context)
//                        .inflate(R.layout.bottom_sheet,(LinearLayout)view.findViewById(R.id.bottom_container));
//                bottomSheetDialog.setContentView(bottomSheetview);
//                bottomSheetDialog.show();
//                final TextView tv_category,tv_kg,tv_price,tv_desc;
//                final ImageView product_img;
//                tv_category=bottomSheetview.findViewById(R.id.txt_category);
//                tv_desc=bottomSheetview.findViewById(R.id.txt_description);
//                product_img=bottomSheetview.findViewById(R.id.category_img);
//                tv_category.setText(productList.getP_name());
//                tv_desc.setText(productList.getP_description());
//                Picasso.with(context).load(Keys.PRODUCT_PATH + productList.getP_photo()).into(product_img);
//                product_img.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        AlertDialog.Builder builder=new AlertDialog.Builder(context,R.style.myFullscreenAlertDialogStyle);
//                        View bottomSheetview= LayoutInflater.from(context)
//                                .inflate(R.layout.custom_alert,(LinearLayout)view.findViewById(R.id.alert_container));
//                        builder.setView(bottomSheetview);
//                        final AlertDialog dialog = builder.create();
//                        dialog.show();
//                        PhotoView photoView;
//                        ImageView imageView;
//                        imageView=bottomSheetview.findViewById(R.id.img_cancel);
//                        imageView.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                dialog.dismiss();
//                            }
//                        });
//                        photoView=(PhotoView)bottomSheetview.findViewById(R.id.categoryImg);
//                        Picasso.with(context).load(Keys.PRODUCT_PATH + productList.getP_photo()).into(photoView);
//                    }
//                });


//                tv_kg=bottomSheetview.findViewById(R.id.txt_kg);
//                tv_price=bottomSheetview.findViewById(R.id.txt_price);

            }
        });
    }

    private void suggestionProducts(String u_id, String p_id, ViewHolder holder) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.SUGGESTION_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("suggestions",response);

                try {
                    JSONArray jsonObject=new JSONArray(response);

//                    if (jsonObject.getString("success").equals("1")){
//                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                       JSONObject jsonObject1=jsonObject.getJSONObject(0);
                        JSONArray jsonArray=jsonObject1.getJSONArray("data");

                        String data="";
                        for (int i=0;i<jsonArray.length();i++){
                            if (i+1==jsonArray.length()){
                                data += jsonArray.getString(i);
                            }else {
                                data += jsonArray.getString(i) + ",";
                            }

                        }

                    showDetails(data,holder);
                } catch (JSONException e) {
                    e.printStackTrace();
                  //  Toast.makeText(context, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_id",u_id);
                params.put("p_id",p_id);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void showDetails(String jsonArray, ViewHolder holder) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.SUGGESTION_PRODUCT_DETAILS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("productDetails",response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        holder.suggestion_layouts.setVisibility(View.VISIBLE);
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            suggestionList.add(new ProductList(
                                    jsonObject1.getString("p_id"),
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("p_name"),
                                    jsonObject1.getString("p_description"),
                                    jsonObject1.getString("p_quantity"),
                                    jsonObject1.getString("p_price"),
                                    jsonObject1.getString("p_dis_type"),
                                    jsonObject1.getString("p_discount"),
                                    jsonObject1.getString("un_id"),
                                    jsonObject1.getString("p_photo"),
                                    jsonObject1.getString("p_enable"),
                                    jsonObject1.getString("un_name"),
                                    jsonObject1.getString("c_name")));
                        }
                        LinearLayoutManager layoutManager1=new LinearLayoutManager(context.getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                        holder.recyclerView_suggestion.setLayoutManager(layoutManager1);
                        holder.recyclerView_suggestion.setHasFixedSize(true);
                        SuggestionAdapter adapter=new SuggestionAdapter(context.getApplicationContext(),suggestionList);
                        holder.recyclerView_suggestion.setAdapter(adapter);
                        holder.suggestion_layouts.setVisibility(View.VISIBLE);

                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }else {
                       // Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("cart",jsonArray.toString());
                Log.i("nik",jsonArray.toString());
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void showSnack(View view) {
        Snackbar snackBar = Snackbar.make(view, "Item added to cart", Snackbar.LENGTH_LONG) .setAction("View Cart", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, MyCartActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        snackBar.setActionTextColor(Color.WHITE);
        View snackBarView = snackBar.getView();
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackBar.show();
    }

    private void increaseInteger(View view, ViewHolder holder) {
//        if (counter == 5) {
//
//        } else {
        if (counter > -1) {
            counter = counter + 1;
            display(counter, holder,"increase");
        }
        // }
    }

    public void decreaseInteger(View view, ViewHolder holder) {
        if (counter < 1) {

        } else {
            counter = counter - 1;
            display(counter, holder,"decrease");
            if (counter==0) {
                counter = 1;
                holder.tv_cartCount.setText("1");
                holder.button_buy.setVisibility(View.VISIBLE);
                holder.llCartAddRemove.setVisibility(View.GONE);
                CartModel.open();
                SQLiteDatabase db = CartModel.dbHelper.getWritableDatabase();
                db.delete(Keys.CARTTABLE.TB_NAME, "p_id = ?", new String[]{productList.getP_id()});
                Log.i("nik","value decrement updated");
                CartModel.close();
            }
        }

    }

    private void display(int number, ViewHolder holder,String type) {
        holder.tv_cartCount.setText("" + number);
        String cartItem=holder.tv_cartCount.getText().toString();
        if (type.equals("decrease")){



        }



    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, ivRemove, ivadd;
        TextView tv_name, tv_new_price,tv_old_price,tv_kg, tv_cartCount,tv_offer,tv_stock;
        LinearLayout cd;
        ImageView button_buy;
        LinearLayout llCartAddRemove,suggestion_layouts;
        RecyclerView recyclerView_suggestion;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.p_image);
            tv_name = itemView.findViewById(R.id.p_name);
            tv_kg = itemView.findViewById(R.id.p_kg);
            tv_new_price = itemView.findViewById(R.id.txt_new_price);
            tv_old_price=itemView.findViewById(R.id.txt_old_price);
            tv_offer=itemView.findViewById(R.id.txt_offer);
            button_buy=itemView.findViewById(R.id.btn_buy);
            llCartAddRemove = itemView.findViewById(R.id.linearLayoutCartAddRemove);
            ivRemove = itemView.findViewById(R.id.imageViewPremovefromcart);
            ivadd = itemView.findViewById(R.id.imageViewPaddfromcart);
            tv_cartCount = itemView.findViewById(R.id.textViewPCartCount);
            recyclerView_suggestion=itemView.findViewById(R.id.recycler_suggestion_product);
            cd=itemView.findViewById(R.id.llCard);
            tv_stock=itemView.findViewById(R.id.tvStock);
            suggestion_layouts=itemView.findViewById(R.id.suggestion_layout);
        }
    }
    public  static void bindListener(com.mindscript.categoryclick Listener){
        categoryclick=Listener;
    }
    public  static void bindListener(com.mindscript.productclick Listener){
        productclick=Listener;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                Log.i("nik",constraint+"");
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ProductList> results = new ArrayList<ProductList>();
                if (orig == null)
                    orig = list;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {

                        for (final ProductList g : orig) {
                            // Log.i("nik",g.get.toString());
                            // ArrayList<String> keyword = new ArrayList<String>(Arrays.asList(g.getB_keywords().split(",")));
                            if (g.getP_name().toLowerCase()
                                    .contains(constraint.toString().toLowerCase())) {
                                results.add(g);
                                Log.i("nik",g+"");
                            }

                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                list = (ArrayList<ProductList>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}