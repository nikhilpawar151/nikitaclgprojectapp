package com.mindscript.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.mindscript.pojo.CategoryList;
import com.mindscript.pojo.ProductNameList;
import com.mindscript.shopyouneeds.ProductActivity;
import com.mindscript.shopyouneeds.R;
import com.mindscript.util.Keys;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {
    Context context;
    ArrayList<CategoryList> list=new ArrayList<>();
    boolean isShimmer = true;
    int SHIMMER_ITEM=3;



    public CategoryListAdapter(Context context, ArrayList<CategoryList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View listItem = layoutInflater.inflate(R.layout.custom_category, parent, false);
        listItem.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setTag(list.get(position));
        final CategoryList category =list.get(position);
        ArrayList<ProductNameList> productName = category.getAl();
            Picasso.with(context).load(Keys.CATEGORY_PATH+category.getC_photo()).into(holder.imageView);
            holder.tv_category.setText(category.getC_name());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5, 0, 5, 0);
            holder.tv_category.setLayoutParams(params);
            ArrayList<String> names=new ArrayList<>();
            String s = "";
            for (int i=0;i<productName.size();i++){
                holder.tv_p_name.append(productName.get(i).getP_name()+", ");
//                names.add(productName.get(0).getP_name());
//                names.add(productName.get(1).getP_name());
//                names.add(productName.get(2).getP_name());
//                names.add(productName.get(3).getP_name());
//                names.add(productName.get(4).getP_name());
            }

            //holder.tv_p_name.setText(names);
//            for (int i=0;i<productName.size();i++){
//                holder.tv_p_name.setText(productName.get(0).getP_name());
//            }
//            if(productName.size()>1){
//                for (int i=0;i<productName.size();i++) {
//                    holder.tv_p_name.setText(productName.get(1).getP_name());
//                }
//            }
            holder.tv_offer.setText("Upto "+category.getP_discount()+"% Off");
            holder.tv_offer.setTextColor(context.getResources().getColor(R.color.green));
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params1.setMargins(5, 0, 5, 0);
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params2.setMargins(5, 0, 5, 0);
            holder.tv_offer.setLayoutParams(params1);
            holder.tv_p_name.setLayoutParams(params2);
            holder.imageView.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            holder.tv_p_name.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            holder.tv_offer.setBackgroundColor(context.getResources().getColor(R.color.transparent));
            holder.tv_category.setBackgroundColor(context.getResources().getColor(R.color.transparent));


        holder.cd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ProductActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("search",category.getC_id());
                intent.putExtra("c_name",category.getC_name());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tv_category,tv_offer,tv_p_name;
        CardView cd;
        LinearLayout linearLayout;
        ShimmerFrameLayout shimmerFrameLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.img_category);
            tv_category=itemView.findViewById(R.id.txt_category_name);
            linearLayout=itemView.findViewById(R.id.categoty_layout);
            cd=itemView.findViewById(R.id.cd_category);
            tv_offer=itemView.findViewById(R.id.txt_offer);
            tv_p_name=itemView.findViewById(R.id.txt_product_name);


        }
    }

}
