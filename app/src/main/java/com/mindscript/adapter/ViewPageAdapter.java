package com.mindscript.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.mindscript.pojo.BannerList;
import com.mindscript.shopyouneeds.R;
import com.mindscript.util.Keys;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ViewPageAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    ArrayList<BannerList> list;

    public ViewPageAdapter(Context context, ArrayList<BannerList> list) {
        this.context = context;
        this.list=list;

    }


    @Override
    public int getCount() {
        return list.size();
        //  return images.length;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.custom_layout,null);
        ImageView imageView=view.findViewById(R.id.imageView);
        BannerList bannerList=list.get(position);
        //imageView.setImageResource(images[position]);

        Picasso.with(context).load(Keys.IMAGE_PATH+bannerList.getB_image()).into(imageView);
        ViewPager vp=(ViewPager) container;
        vp.addView(view,0);
        return view;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager vp=(ViewPager)container;
        View view=(View)object;
        vp.removeView(view);

    }








}
