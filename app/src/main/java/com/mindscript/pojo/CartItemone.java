package com.mindscript.pojo;

public class CartItemone {
    private String id;
    private String p_name;
    private String p_price;
    private String p_dis_type;
    private String p_discount;
    private String p_quantity;
    private String un_id;
    private String p_photo;
    private String un_name;
    private String u_quantity;

    public CartItemone(String id, String p_name, String p_price, String p_dis_type, String p_discount, String p_quantity, String un_id, String p_photo, String un_name, String u_quantity) {
        this.id = id;
        this.p_name = p_name;
        this.p_price = p_price;
        this.p_dis_type = p_dis_type;
        this.p_discount = p_discount;
        this.p_quantity = p_quantity;
        this.un_id = un_id;
        this.p_photo = p_photo;
        this.un_name = un_name;
        this.u_quantity = u_quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_price() {
        return p_price;
    }

    public void setP_price(String p_price) {
        this.p_price = p_price;
    }

    public String getP_dis_type() {
        return p_dis_type;
    }

    public void setP_dis_type(String p_dis_type) {
        this.p_dis_type = p_dis_type;
    }

    public String getP_discount() {
        return p_discount;
    }

    public void setP_discount(String p_discount) {
        this.p_discount = p_discount;
    }

    public String getP_quantity() {
        return p_quantity;
    }

    public void setP_quantity(String p_quantity) {
        this.p_quantity = p_quantity;
    }

    public String getUn_id() {
        return un_id;
    }

    public void setUn_id(String un_id) {
        this.un_id = un_id;
    }

    public String getP_photo() {
        return p_photo;
    }

    public void setP_photo(String p_photo) {
        this.p_photo = p_photo;
    }

    public String getUn_name() {
        return un_name;
    }

    public void setUn_name(String un_name) {
        this.un_name = un_name;
    }

    public String getU_quantity() {
        return u_quantity;
    }

    public void setU_quantity(String u_quantity) {
        this.u_quantity = u_quantity;
    }
}
