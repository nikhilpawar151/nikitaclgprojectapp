package com.mindscript.pojo;

public class BannerList {
    private String b_id;
    private String b_image;
    private String b_status;

    public BannerList(String b_id, String b_image, String b_status) {
        this.b_id = b_id;
        this.b_image = b_image;
        this.b_status = b_status;
    }

    public String getB_id() {
        return b_id;
    }

    public void setB_id(String b_id) {
        this.b_id = b_id;
    }

    public String getB_image() {
        return b_image;
    }

    public void setB_image(String b_image) {
        this.b_image = b_image;
    }

    public String getB_status() {
        return b_status;
    }

    public void setB_status(String b_status) {
        this.b_status = b_status;
    }
}
