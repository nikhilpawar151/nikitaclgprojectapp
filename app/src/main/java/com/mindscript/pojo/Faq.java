package com.mindscript.pojo;

public class Faq {
    String id;
    String ques;
    String ans;

    public Faq(String id, String ques, String ans) {
        this.id = id;
        this.ques = ques;
        this.ans = ans;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQues() {
        return ques;
    }

    public void setQues(String ques) {
        this.ques = ques;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }
}
