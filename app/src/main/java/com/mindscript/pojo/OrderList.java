package com.mindscript.pojo;

public class OrderList {
    private String o_id;
    private String o_no;

    public OrderList(String o_id, String o_no, String o_date, String o_name, String o_phone, String o_address, String o_pin, String o_email,String o_total, String o_status) {
        this.o_id = o_id;
        this.o_no = o_no;
        this.o_date = o_date;
        this.o_name = o_name;
        this.o_phone = o_phone;
        this.o_address = o_address;
        this.o_pin = o_pin;
        this.o_email = o_email;
        this.o_total = o_total;
        this.o_status = o_status;
    }

    private String o_date;

    public String getO_name() {
        return o_name;
    }

    public void setO_name(String o_name) {
        this.o_name = o_name;
    }

    public String getO_phone() {
        return o_phone;
    }

    public void setO_phone(String o_phone) {
        this.o_phone = o_phone;
    }

    public String getO_address() {
        return o_address;
    }

    public void setO_address(String o_address) {
        this.o_address = o_address;
    }

    public String getO_pin() {
        return o_pin;
    }

    public void setO_pin(String o_pin) {
        this.o_pin = o_pin;
    }

    public String getO_email() {
        return o_email;
    }

    public void setO_email(String o_email) {
        this.o_email = o_email;
    }



    public String getO_total() {
        return o_total;
    }

    public void setO_total(String o_total) {
        this.o_total = o_total;
    }

    public String getO_status() {
        return o_status;
    }

    public void setO_status(String o_status) {
        this.o_status = o_status;
    }

    private String o_name;
    private String o_phone;
    private String o_address;
    private String o_pin;
    private String o_email;

    private String o_total;
    private String o_status;


    public String getO_id() {
        return o_id;
    }

    public void setO_id(String o_id) {
        this.o_id = o_id;
    }

    public String getO_no() {
        return o_no;
    }

    public void setO_no(String o_no) {
        this.o_no = o_no;
    }

    public String getO_date() {
        return o_date;
    }

    public void setO_date(String o_date) {
        this.o_date = o_date;
    }
}
