package com.mindscript.pojo;

public class OrderDetails {
    int od_id, o_id, p_id, p_quantity, un_id;
    String p_name;

    public int getOd_id() {
        return od_id;
    }

    public void setOd_id(int od_id) {
        this.od_id = od_id;
    }

    public int getO_id() {
        return o_id;
    }

    public void setO_id(int o_id) {
        this.o_id = o_id;
    }

    public int getP_id() {
        return p_id;
    }

    public void setP_id(int p_id) {
        this.p_id = p_id;
    }

    public int getP_quantity() {
        return p_quantity;
    }

    public void setP_quantity(int p_quantity) {
        this.p_quantity = p_quantity;
    }

    public int getUn_id() {
        return un_id;
    }

    public void setUn_id(int un_id) {
        this.un_id = un_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_rate() {
        return p_rate;
    }

    public void setP_rate(String p_rate) {
        this.p_rate = p_rate;
    }

    public String getUn_name() {
        return un_name;
    }

    public void setUn_name(String un_name) {
        this.un_name = un_name;
    }

    public String getP_photo() {
        return p_photo;
    }

    public void setP_photo(String p_photo) {
        this.p_photo = p_photo;
    }


    String p_rate;
    String un_name;

    public OrderDetails(int od_id, int o_id, int p_id, int p_quantity,int un_id, String p_name, String p_rate,
                        String un_name, String p_photo) {
        this.od_id = od_id;
        this.o_id = o_id;
        this.p_id = p_id;
        this.p_quantity = p_quantity;
        this.un_id = un_id;
        this.p_name = p_name;
        this.p_rate = p_rate;
        this.un_name = un_name;
        this.p_photo = p_photo;
    }

    String p_photo;
}
