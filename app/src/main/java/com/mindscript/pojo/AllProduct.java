package com.mindscript.pojo;

public class AllProduct {

    private String p_id;
    private String c_id;
    private String p_name;
    private String p_description;
    private String p_price;
    private String p_quantity;
    private String p_dis_type;
    private String p_discount;
    private String un_id;
    private String p_photo;
    private String p_enable;
    private String un_name;


    public AllProduct(String p_id, String c_id, String p_name, String p_description, String p_price, String p_quantity, String p_dis_type, String p_discount, String un_id, String p_photo, String p_enable, String un_name) {
        this.p_id = p_id;
        this.c_id = c_id;
        this.p_name = p_name;
        this.p_description = p_description;
        this.p_price = p_price;
        this.p_quantity = p_quantity;
        this.p_dis_type = p_dis_type;
        this.p_discount = p_discount;
        this.un_id = un_id;
        this.p_photo = p_photo;
        this.p_enable = p_enable;
        this.un_name = un_name;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_description() {
        return p_description;
    }

    public void setP_description(String p_description) {
        this.p_description = p_description;
    }

    public String getP_price() {
        return p_price;
    }

    public void setP_price(String p_price) {
        this.p_price = p_price;
    }

    public String getP_quantity() {
        return p_quantity;
    }

    public void setP_quantity(String p_quantity) {
        this.p_quantity = p_quantity;
    }

    public String getP_dis_type() {
        return p_dis_type;
    }

    public void setP_dis_type(String p_dis_type) {
        this.p_dis_type = p_dis_type;
    }

    public String getP_discount() {
        return p_discount;
    }

    public void setP_discount(String p_discount) {
        this.p_discount = p_discount;
    }

    public String getUn_id() {
        return un_id;
    }

    public void setUn_id(String un_id) {
        this.un_id = un_id;
    }

    public String getP_photo() {
        return p_photo;
    }

    public void setP_photo(String p_photo) {
        this.p_photo = p_photo;
    }

    public String getP_enable() {
        return p_enable;
    }

    public void setP_enable(String p_enable) {
        this.p_enable = p_enable;
    }

    public String getUn_name() {
        return un_name;
    }

    public void setUn_name(String un_name) {
        this.un_name = un_name;
    }
}
