package com.mindscript.pojo;

import java.util.ArrayList;

public class CategoryList {

    private String c_id;
    private String c_name;
    private String c_photo;
    private String c_enable;
    private String p_discount;
    ArrayList<ProductNameList> al;

    public CategoryList(String c_id, String c_name, String c_photo, String c_enable, String p_discount, ArrayList<ProductNameList> al) {
        this.c_id = c_id;
        this.c_name = c_name;
        this.c_photo = c_photo;
        this.c_enable = c_enable;
        this.p_discount = p_discount;
        this.al = al;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_photo() {
        return c_photo;
    }

    public void setC_photo(String c_photo) {
        this.c_photo = c_photo;
    }

    public String getC_enable() {
        return c_enable;
    }

    public void setC_enable(String c_enable) {
        this.c_enable = c_enable;
    }

    public String getP_discount() {
        return p_discount;
    }

    public void setP_discount(String p_discount) {
        this.p_discount = p_discount;
    }

    public ArrayList<ProductNameList> getAl() {
        return al;
    }

    public void setAl(ArrayList<ProductNameList> al) {
        this.al = al;
    }
}
