package com.mindscript.pojo;

public class CartItem {
    private int images;
    private String title;
    private String kg;
    private String price;

    public CartItem(int images, String title, String kg, String price) {
        this.images = images;
        this.title = title;
        this.kg = kg;
        this.price = price;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKg() {
        return kg;
    }

    public void setKg(String kg) {
        this.kg = kg;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
