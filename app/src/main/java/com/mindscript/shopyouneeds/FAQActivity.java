package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.mindscript.adapter.FaqAdapter;
import com.mindscript.pojo.Faq;

import java.util.ArrayList;

public class FAQActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Faq> alFaq=new ArrayList<>();
    FaqAdapter faqAdapter;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f_a_q);
        imageView=findViewById(R.id.back_btn);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recyclerView=findViewById(R.id.recyclerViewFaq);
        LinearLayoutManager layoutManager1=new LinearLayoutManager(FAQActivity.this);
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setHasFixedSize(true);

        alFaq.add(new Faq("","How do I know my order has been confirmed?","You will get a  notification on your mobile phone. You can track your orders from the 'My Orders' section on your Shop Your Needs account."));
        alFaq.add(new Faq("","I missed the delivery of my order today. What should I do?","The courier service delivering your order usually tries to deliver on the next business day in case you miss a delivery. You can check your SMS for more details on when the courier service will try to deliver again."));
        alFaq.add(new Faq("","Will the delivery be tried again if I'm not able to collect my order the first time?","Couriers make sure that the delivery is re-attempted the next working day if you can't collect your order the first time."));
        alFaq.add(new Faq("","What should I do if my order is approved but hasn't been shipped yet?","Sellers usually ship orders 1-2 business days before the delivery date so that they reach you on time. In case your order hasn't been shipped within this time please contact on shopyourneeds@263 so that we can look into it."));
        alFaq.add(new Faq("","I paid cash on delivery, how would I get the refund?","If you chose to pay Cash on Delivery, the refund will be credited in the form of NEFT transfer to yourbank account. For an NEFT transfer we would require the following details:\n" +
                "\n" +
                "1. Bank Name, Account Holder Name, Bank Account Number, Branch Address, IFSC Code\n" +
                "\n" +
                "2. You can write above details to shopyourneeds@263.in"));
        alFaq.add(new Faq("","How do I check the status of my order?","A Visit the MY ORDERS page in your My Account and click on ORDER to get real time status of your order."));
        alFaq.add(new Faq("","What are the modes of payment for purchasing?","We accept the following modes of payment:\n" +
                "\n" +
                "1. Cash"));
        alFaq.add(new Faq("","What are the benefits of creating an account on Shop Your Needs?","A Creating an account on shop your needs is beneficial in many ways. You will have access to:\n" +
                "\n" +
                "1. My Orders - Quickly and easily manage your orders such as View Details, Track etc.\n" +
                "\n" +
                "2. Recommendations For You - Based on your past orders, we recommend products especially suited for you"));

        faqAdapter=new FaqAdapter(alFaq,getApplicationContext());
        recyclerView.setAdapter(faqAdapter);
    }
}