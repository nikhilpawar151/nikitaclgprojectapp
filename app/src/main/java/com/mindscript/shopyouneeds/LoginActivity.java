package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;
import com.mindscript.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText editText_phone,editText_password;
    LinearLayout button_login;
    ProgressBar progressBar;
    Button button_register;
    RelativeLayout relativeLayout;
    TextView tv_register,tv_forgot;
    GoogleSignInClient mgoogleSignInClient;
    int RC_SIGN_IN=1000;
    LinearLayout llGoogleSignIn;
    String name,email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        editText_phone=findViewById(R.id.edt_lphone);
        editText_password=findViewById(R.id.edt_password);
        button_login=findViewById(R.id.btn_login);
        tv_register=findViewById(R.id.text_register);
        tv_forgot=findViewById(R.id.tv_forgot);
        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);

            }
        });
        relativeLayout=findViewById(R.id.progress_login);
        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);


            }
        });
        llGoogleSignIn = findViewById(R.id.llGoogleSignIn);

        llGoogleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signIn();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mgoogleSignInClient= GoogleSignIn.getClient(this,gso);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String u_phone=editText_phone.getText().toString().trim();
                String u_password=editText_password.getText().toString().trim();
                if (u_phone.equals("") || u_password.equals("")){
                    Toast.makeText(LoginActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else {
                    login(u_phone,u_password);
                }
            }
        });
    }

    private void signIn() {
        Intent signInIntent = mgoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("vic",requestCode+"data"+data+"vicky");
        relativeLayout.setVisibility(View.GONE);
        if (requestCode == RC_SIGN_IN && resultCode==Activity.RESULT_OK) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            task = GoogleSignIn.getSignedInAccountFromIntent(data);
            relativeLayout.setVisibility(View.GONE);
            handleSignInResult(task);

        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

        try {
//            progressBar.setVisibility(View.GONE);
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Toast.makeText(LoginActivity.this,"Sign in successfully",Toast.LENGTH_LONG).show();
            account=GoogleSignIn.getLastSignedInAccount(getApplicationContext());
            if (account != null){
//                progressBar.setVisibility(View.GONE);
                name=account.getDisplayName();
                email=account.getEmail();
                checkemail(email);
                //checkemail(email);
            }


//            Intent intent=new Intent(LoginActivity.this,ProfileActivity.class);
//            intent.putExtra("name",name);
//            intent.putExtra("email",email);
//            startActivity(intent);
//            finish();
            // updateUI(account);

            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            relativeLayout.setVisibility(View.GONE);
            e.printStackTrace();
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
            Log.w("Nik", "signInResult:failed code=" + e.getStatusCode());
            // updateUI(null);

        }
    }

    private void checkemail(String email) {
        relativeLayout.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CHECK_EMAIL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("vic","checkemail"+response);
                relativeLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(LoginActivity.this,ProfileCompleteActivity.class);
                        intent.putExtra("name",name);
                        intent.putExtra("email",email);
                        startActivity(intent);
                        finish();
                    }else if(jsonObject.getString("success").equals("2")){
                        String u_phone=jsonObject.getString("u_phone");
                        String u_id=jsonObject.getString("u_id");
                        String u_name=jsonObject.getString("u_name");
                        String u_address=jsonObject.getString("u_address");
                        String u_city=jsonObject.getString("u_city");
                        String u_pin=jsonObject.getString("u_pincode");
                        String u_email=jsonObject.getString("u_email");
                        Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                        SharedPreference.save("u_id",u_id);
                        SharedPreference.save("u_email",u_email);
                        SharedPreference.save("u_phone",u_phone);
                        SharedPreference.save("u_name",u_name);
                        SharedPreference.save("u_phone",u_phone);
                        SharedPreference.save("u_address",u_address);
                        SharedPreference.save("u_city",u_city);
                        SharedPreference.save("u_pin",u_pin);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                relativeLayout.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("u_email", email);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void login(final String u_phone, final String u_password) {
        relativeLayout.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik",response);
                relativeLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String u_id=jsonObject1.getString("u_id");
                        String u_name=jsonObject1.getString("u_name");
                        String u_phone=jsonObject1.getString("u_phone");
                        String u_address=jsonObject1.getString("u_address");
                        String u_city=jsonObject1.getString("u_city");
                        String u_pin=jsonObject1.getString("u_pincode");
                        String u_email=jsonObject1.getString("u_email");
                        String name=u_name;
                        String address=u_address+", "+u_city;

                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        SharedPreference.save("u_id",u_id);
                        SharedPreference.save("u_email",u_email);
                        SharedPreference.save("u_name",name);
                        SharedPreference.save("u_phone",u_phone);
                        SharedPreference.save("u_address",address);
                        SharedPreference.save("u_pin",u_pin);
                        Intent intent;
//                        if (SharedPreference.contains("place_order")){
//                            intent=new Intent(LoginActivity.this,MyCartActivity.class);
//                        }else {
                        intent=new Intent(LoginActivity.this,MainActivity.class);
//                        }
                        startActivity(intent);
                        clear();
                        finish();


                    }else {
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                relativeLayout.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_phone",u_phone);
                params.put("u_password",u_password);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void clear() {
        editText_phone.setText("");
        editText_password.setText("");

    }
//    @Override
//    public void onBackPressed() {
//        Intent intent;
//        if (SharedPreference.contains("place_order")){
//            intent=new Intent(LoginActivity.this,MyCartActivity.class);
//            startActivity(intent);
//        }else {
//
//        }
//        finish();
//    }
}