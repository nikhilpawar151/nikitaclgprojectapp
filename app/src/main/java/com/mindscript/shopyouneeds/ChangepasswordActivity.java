package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangepasswordActivity extends AppCompatActivity {

    EditText editText_phone,editText_password,editText_otp;
    Button button_submit;
    RelativeLayout progressBar;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        imageView=findViewById(R.id.back_btn);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ChangepasswordActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        editText_password=findViewById(R.id.edt_n_password);
        editText_phone=findViewById(R.id.edt_phone);
        editText_otp=findViewById(R.id.edt_otp);
        progressBar=findViewById(R.id.progress_login);
        final Intent i=getIntent();
        final String email=i.getStringExtra("u_email");
        editText_phone.setText(email);
        button_submit=findViewById(R.id.btn_submit);
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String u_otp=editText_otp.getText().toString().trim();
                String u_password=editText_password.getText().toString().trim();
                if (u_password.equals("") || u_otp.equals("")){
                    Toast.makeText(ChangepasswordActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else {
                    forgotPassword(email,u_otp,u_password);
                }
            }
        });
    }
    private void forgotPassword(final String email, final String u_otp, final String u_password) {
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CHANGE_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("pri",response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getString("success").equals("1")){
                        Toast.makeText(ChangepasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(ChangepasswordActivity.this,LoginActivity.class);
                        clear();
                        startActivity(i);
                        finish();

                    }else {
                        Toast.makeText(ChangepasswordActivity.this,  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_email",email);
                params.put("u_otp",u_otp);
                params.put("u_password",u_password);
                return  params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void clear() {
        editText_phone.setText("");
        editText_otp.setText("");
        editText_password.setText("");
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(ChangepasswordActivity.this,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}