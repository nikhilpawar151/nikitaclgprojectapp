package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mindscript.adapter.MyCartAdapter;
import com.mindscript.categoryclick;
import com.mindscript.pojo.CartItemone;
import com.mindscript.util.AppController;
import com.mindscript.util.CartModel;
import com.mindscript.util.Keys;
import com.mindscript.util.SharedPreference;

import java.util.ArrayList;

public class MyCartActivity extends AppCompatActivity {


    RecyclerView recyclerView_cart;
    ArrayList<CartItemone> list;
    String id,name,p_quantity,photo,price,quantity,u_id,un_id,p_dis_type,p_discount,un_name;
    ImageView imageView;
    LinearLayout llCartEmpty;
    RelativeLayout placelayout;
    MyCartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        list=new ArrayList<>();
        recyclerView_cart=findViewById(R.id.recycler_my_cart);
        llCartEmpty=findViewById(R.id.linearLayoutCartempty);
        placelayout=findViewById(R.id.place_layout);
        imageView=findViewById(R.id.back_btn);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent=new Intent(MyCartActivity.this,ProductCategoryActivity.class);
//                startActivity(intent);
                if (SharedPreference.contains("place_order")){
                    SharedPreference.removeKey("place_order");
                    Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
                    startActivity(intent);

                }else{
                    Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }


            }
        });
        u_id= SharedPreference.get("u_id");
        CartModel.getInstance(getApplicationContext());
        CartModel.open();
        // Cursor cursor = CartModel.database.rawQuery("select * from "+ Keys.CARTTABLE.TB_NAME,null);

        Cursor cursor = CartModel.database.rawQuery("select * from "+  Keys.CARTTABLE.TB_NAME,null);
        while(cursor.moveToNext()) {
            id = cursor.getString(2);   //0 is the number of id column in your database table
            name = cursor.getString(4);
            price = cursor.getString(5);
            p_dis_type=cursor.getString(6);
            p_discount=cursor.getString(7);
            p_quantity= cursor.getString(8);
            un_id=cursor.getString(9);
            photo=cursor.getString(10);
            un_name=cursor.getString(11);
            quantity = cursor.getString(12);

            CartItemone cartItemone =new CartItemone(id,name,price,p_dis_type,p_discount,p_quantity,un_id,photo,un_name,quantity);
            list.add(cartItemone);

        }
        CartModel.close();

        placelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (SharedPreference.contains("u_id")) {
                Intent intent = new Intent(MyCartActivity.this, PlaceOrderActivity.class);
                startActivity(intent);
//                }else {
//                    SharedPreference.save("place_order","place_order");
//                    Intent intent = new Intent(MyCartActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//                }


            }
        });
        cartAdapter=new MyCartAdapter(MyCartActivity.this,list);
        MyCartAdapter.bindListener(new categoryclick() {
            @Override
            public void click(String c_id) {
                list.clear();
                CartModel.getInstance(getApplicationContext());
                CartModel.open();
                // Cursor cursor = CartModel.database.rawQuery("select * from "+ Keys.CARTTABLE.TB_NAME,null);

                Cursor cursor = CartModel.database.rawQuery("select * from "+  Keys.CARTTABLE.TB_NAME ,null);
                while(cursor.moveToNext()) {
                    id = cursor.getString(2);   //0 is the number of id column in your database table
                    name = cursor.getString(4);
                    price = cursor.getString(5);
                    p_dis_type=cursor.getString(6);
                    p_discount=cursor.getString(7);
                    p_quantity= cursor.getString(8);
                    un_id=cursor.getString(9);
                    photo=cursor.getString(10);
                    un_name=cursor.getString(11);
                    quantity = cursor.getString(12);
                    CartItemone cartItemone =new CartItemone(id,name,price,p_dis_type,p_discount,p_quantity,un_id,photo,un_name,quantity);
                    list.add(cartItemone);

                }
                cartAdapter.notifyDataSetChanged();
                CartModel.close();
                if (list.isEmpty()){
                    llCartEmpty.setVisibility(View.VISIBLE);
                    placelayout.setVisibility(View.GONE);
                }else {
                    llCartEmpty.setVisibility(View.GONE);
                    placelayout.setVisibility(View.VISIBLE);
                }

            }
        });
//        list.add(new CartItem(R.drawable.milk,"Milk","1 KG","₹60"));
//        list.add(new CartItem(R.drawable.vegetables,"Vegetable","2 KG","₹90"));
        LinearLayoutManager layoutManager1=new LinearLayoutManager(MyCartActivity.this);
        recyclerView_cart.setLayoutManager(layoutManager1);
        recyclerView_cart.setHasFixedSize(true);
        cartAdapter=new MyCartAdapter(MyCartActivity.this,list);
        recyclerView_cart.setAdapter(cartAdapter);
        if (list.isEmpty()){
            llCartEmpty.setVisibility(View.VISIBLE);
            placelayout.setVisibility(View.GONE);
        }else {
            llCartEmpty.setVisibility(View.GONE);
            placelayout.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
        startActivity(intent);
        finish();

    }
}