package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;
import com.mindscript.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileCompleteActivity extends AppCompatActivity {

    EditText editText_first,editText_email,editText_pass,editText_conf_pass,editText_phone,editText_address,
            editText_city,editText_pincode;
    LinearLayout button_register;
    RelativeLayout progressbar;
    String name,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_complete);

        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());

        editText_first=findViewById(R.id.edt_first_name);
        editText_email=findViewById(R.id.edt_email);
        editText_pass=findViewById(R.id.edt_password);
        editText_conf_pass=findViewById(R.id.edt_conf_password);
        editText_phone=findViewById(R.id.edt_phone);
        editText_address=findViewById(R.id.edt_address);
        editText_city=findViewById(R.id.edt_city);
        editText_pincode=findViewById(R.id.edt_pincode);
        button_register=findViewById(R.id.btn_register);
        progressbar=findViewById(R.id.progress_login);
        Intent i=getIntent();
        name=i.getStringExtra("name");
        email=i.getStringExtra("email");
        editText_first.setText(name);
        editText_email.setText(email);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.background); //bg_gradient is your gradient.
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String u_name=editText_first.getText().toString().trim();
                String u_email=editText_email.getText().toString().trim();
                String u_phone=editText_phone.getText().toString().trim();
                String u_address=editText_address.getText().toString().trim();
                String u_city=editText_city.getText().toString().trim();
                String u_pincode=editText_pincode.getText().toString().trim();

                if (u_name.equals("") || u_email.equals("")  || u_phone.equals("") || u_address.equals("") || u_city.equals("") || u_pincode.equals("")){
                    Toast.makeText(ProfileCompleteActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(u_email).matches()){
                    editText_email.setError("Invalid Email ID");
                }else if (u_phone.length()!=10){
                    editText_phone.setError("Phone number must be 10 digits");
                }else if (u_pincode.length()!=6){
                    editText_pincode.setError("PinCode must be 6 digits");
                }else {
                    register(u_name,u_email,u_phone,u_address,u_city,u_pincode);
                }



            }
        });


    }

    private void register(final String u_name, final String u_email,final String u_phone, final String u_address, final String u_city, final String u_pincode) {
        progressbar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.PROFILE_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressbar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        Toast.makeText(ProfileCompleteActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ProfileCompleteActivity.this, MainActivity.class);
                        startActivity(intent);
                        clear();
                        finish();

                    }else {
                        Toast.makeText(ProfileCompleteActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileCompleteActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_name",u_name);
                params.put("u_email",u_email);
                params.put("u_password","0");
                params.put("u_phone",u_phone);
                params.put("u_address",u_address);
                params.put("u_city",u_city);
                params.put("u_pincode",u_pincode);
                params.put("u_type","2");
                return params;
            }
        };
        AppController.getInstance().add(request);


    }

    private void clear() {
        editText_first.setText("");
        editText_email.setText("");
        editText_pass.setText("");
        editText_conf_pass.setText("");
        editText_phone.setText("");
        editText_address.setText("");
        editText_city.setText("");
        editText_pincode.setText("");
    }

    @Override
    public void onBackPressed() {

        finish();

    }
}