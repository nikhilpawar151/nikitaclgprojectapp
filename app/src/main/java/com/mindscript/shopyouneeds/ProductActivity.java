package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.mindscript.adapter.AllProductAdapter;
import com.mindscript.adapter.ProductAdapter;
import com.mindscript.adapter.SuggestionAdapter;
import com.mindscript.categoryclick;
import com.mindscript.pojo.AllProduct;
import com.mindscript.pojo.ProductList;
import com.mindscript.productclick;
import com.mindscript.util.AppController;
import com.mindscript.util.CartModel;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductActivity extends AppCompatActivity {

    ArrayList<ProductList> list;
    ArrayList<ProductList> suggesstionList=new ArrayList<>();
    ProductAdapter adapter;
    ProgressBar progressBar;
    RecyclerView recyclerView_product,recyclerView_suggestion_products;
    TextView tv_category;
    EditText editText_search;
    ImageView viewCart,back_img;
    TextView tvCartCount;
    String value="";
    String url;
    ArrayList<AllProduct> productList;
    AllProductAdapter allProductAdapter;
    LinearLayout layout_empty;
    LinearLayout layout_product;

    SwipeRefreshLayout swipeRefreshLayout;
    SuggestionAdapter suggestionAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        list=new ArrayList<>();
        productList=new ArrayList<>();
        layout_empty = findViewById(R.id.linearLayoutProductempty);
        recyclerView_product=findViewById(R.id.recycler_product_list);
        recyclerView_suggestion_products=findViewById(R.id.recycler_suggestion_product);
        progressBar=findViewById(R.id.progress_list);
        tvCartCount=findViewById(R.id.textViewCartCount);
        tv_category=findViewById(R.id.txt_category);
        viewCart=findViewById(R.id.view_cart);
        back_img=findViewById(R.id.back_btn);
        layout_product=findViewById(R.id.product_layout);
        swipeRefreshLayout=findViewById(R.id.sw_data);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.purple_200));
        String u_id=SharedPreference.get("u_id");


        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        viewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProductActivity.this,MyCartActivity.class);
                startActivity(intent);
            }
        });
        tv_category=findViewById(R.id.txt_category);
        editText_search=findViewById(R.id.edittextPsearch);
        Intent i=getIntent();
        final String search=i.getStringExtra("search");
        if(search.equals("0")){
            value=search;
            url= Keys.URL.ALL_PRODUCT_LIST;
            tv_category.setText("All Products");
            productList(search);
        }else{
            value=search;
            url=Keys.URL.PRODUCT_LIST;
            String c_name=i.getStringExtra("c_name");
            tv_category.setText(c_name);
            productList(search);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                productList.clear();
                if(search.equals("0")){
                    value=search;
                    url= Keys.URL.ALL_PRODUCT_LIST;
                    tv_category.setText("All Products");
                    productList(search);
                }else{
                    value=search;
                    url=Keys.URL.PRODUCT_LIST;
                    String c_name=i.getStringExtra("c_name");
                    tv_category.setText(c_name);
                    productList(search);
                }


            }
        });
//        if (list.isEmpty()) {
//            layout_empty.setVisibility(View.VISIBLE);
//            layout_product.setVisibility(View.GONE);
//        } else {
//            layout_empty.setVisibility(View.GONE);
//            layout_product.setVisibility(View.VISIBLE);
//        }
        //adapter=new ProductAdapter(ProductActivity.this,list);
        //recyclerView_product.setAdapter(adapter);
        LinearLayoutManager layoutManager=new LinearLayoutManager(ProductActivity.this);
        recyclerView_product.setLayoutManager(layoutManager);
        recyclerView_product.setHasFixedSize(true);

        LinearLayoutManager layoutManager1=new LinearLayoutManager(ProductActivity.this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView_suggestion_products.setLayoutManager(layoutManager1);
        recyclerView_suggestion_products.setHasFixedSize(true);



        editText_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.getFilter().filter(editText_search.getText().toString());
            }
        });

        showcartcount();
        ProductAdapter.bindListener(new categoryclick() {
            @Override
            public void click(String c_id) {
                showcartcount();
            }
        });
        ProductAdapter.bindListener(new productclick() {
            @Override
            public void click(String p_id) {
                suggestionProducts(u_id,p_id);
            }
        });



    }

    private void suggestionProducts(String u_id, String p_id) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.SUGGESTION_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("suggestions",response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        showDetails(jsonArray);
                        recyclerView_suggestion_products.setVisibility(View.VISIBLE);

                        Toast.makeText(ProductActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(ProductActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProductActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_id",u_id);
                params.put("p_id",p_id);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void showDetails(JSONArray jsonArray) {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.SUGGESTION_PRODUCT_DETAILS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("productDetails",response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            suggesstionList.add(new ProductList(
                                    jsonObject1.getString("p_id"),
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("p_name"),
                                    jsonObject1.getString("p_description"),
                                    jsonObject1.getString("p_quantity"),
                                    jsonObject1.getString("p_price"),
                                    jsonObject1.getString("p_dis_type"),
                                    jsonObject1.getString("p_discount"),
                                    jsonObject1.getString("un_id"),
                                    jsonObject1.getString("p_photo"),
                                    jsonObject1.getString("p_enable"),
                                    jsonObject1.getString("un_name"),
                                    jsonObject1.getString("c_name")));
                        }
                        recyclerView_suggestion_products.setVisibility(View.VISIBLE);
                        suggestionAdapter=new SuggestionAdapter(ProductActivity.this,suggesstionList);
                        recyclerView_suggestion_products.setAdapter(adapter);
                        suggestionAdapter.notifyDataSetChanged();
                        if (list.isEmpty()) {
                            layout_empty.setVisibility(View.VISIBLE);
                            layout_product.setVisibility(View.GONE);
                        } else {
                            layout_empty.setVisibility(View.GONE);
                            layout_product.setVisibility(View.VISIBLE);
                        }
                        Toast.makeText(ProductActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(ProductActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProductActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("cart",jsonArray.toString());
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void productList(final String c_id) {
        list.clear();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik",response);
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new ProductList(
                                    jsonObject1.getString("p_id"),
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("p_name"),
                                    jsonObject1.getString("p_description"),
                                    jsonObject1.getString("p_quantity"),
                                    jsonObject1.getString("p_price"),
                                    jsonObject1.getString("p_dis_type"),
                                    jsonObject1.getString("p_discount"),
                                    jsonObject1.getString("un_id"),
                                    jsonObject1.getString("p_photo"),
                                    jsonObject1.getString("p_enable"),
                                    jsonObject1.getString("un_name"),
                                    jsonObject1.getString("c_name")));
                        }
                        recyclerView_product.setVisibility(View.VISIBLE);
                        adapter=new ProductAdapter(ProductActivity.this,list);
                        recyclerView_product.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        if (list.isEmpty()) {
                            layout_empty.setVisibility(View.VISIBLE);
                            layout_product.setVisibility(View.GONE);
                        } else {
                            layout_empty.setVisibility(View.GONE);
                            layout_product.setVisibility(View.VISIBLE);
                        }



                    }else {

                        Toast.makeText(ProductActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProductActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                if (value.equals("0")){
                    Loggers.i("No value");
                }else {
                    params.put("c_id",c_id);
                }

                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void showcartcount() {
        CartModel.getInstance(getApplicationContext());
        CartModel.open();
        String u_id= SharedPreference.get("u_id");
        String sql = "select * from "+ Keys.CARTTABLE.TB_NAME;
        Loggers.i(sql);
        Cursor cursor = CartModel.database.rawQuery(sql ,null);
        ArrayList<String> alpc_id=new ArrayList<>();
        if(cursor != null) {
            Loggers.i("======================= " + cursor.getCount());
            while (cursor.moveToNext()) {
                alpc_id.add(cursor.getString(0));
            }
            tvCartCount.setText(alpc_id.size()+"");
        }
        CartModel.close();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (adapter!=null){
            adapter.notifyDataSetChanged();
            showcartcount();
        }
    }
}