package com.mindscript.shopyouneeds;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mindscript.adapter.AllProductAdapter;
import com.mindscript.adapter.ProductAdapter;
import com.mindscript.categoryclick;
import com.mindscript.shopyouneeds.R;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;
import com.mindscript.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    TextView tv_name,tv_email,tv_phone,tv_address;
    String f_name,l_name,email,address,phone,city,pin;
    BottomNavigationView bottomNavigationView;
    RelativeLayout header;
    ImageView imageViewMenu;
    DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout relativeLayout;
    String token;
    GoogleSignInOptions gso;
    TextView tv_order,tv_profile,tv_about,tv_contact,tvUsername,tv_logout,tv_share,tv_rate,tv_faq;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        gso = new GoogleSignInOptions.
                Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                build();
        relativeLayout=findViewById(R.id.progress_profile);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        header=findViewById(R.id.header);
        imageViewMenu = findViewById(R.id.menu_button);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);


        tv_name=findViewById(R.id.txt_name);
        tv_email=findViewById(R.id.txt_email);
        tv_phone=findViewById(R.id.txt_phone);
        tv_address=findViewById(R.id.txt_address);
        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(ProfileActivity.this, "grtvgte", Toast.LENGTH_SHORT).show();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.navigation_person);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View headerLayout = navigationView.getHeaderView(0);
        tvUsername=headerLayout.findViewById(R.id.textViewNavigateMenuUsername);
        tv_profile=headerLayout.findViewById(R.id.txt_profile);
        tv_order=headerLayout.findViewById(R.id.txt_orders);
        tv_about=headerLayout.findViewById(R.id.txt_about);
        tv_contact=headerLayout.findViewById(R.id.txt_contact);
        tv_logout=headerLayout.findViewById(R.id.txt_logout);
        tv_share=headerLayout.findViewById(R.id.txt_shareapp);
        tv_rate=headerLayout.findViewById(R.id.txt_rateus);
        tv_faq=headerLayout.findViewById(R.id.txt_faq);

        if (SharedPreference.contains("u_id")) {
            tvUsername.setText(SharedPreference.get("u_name"));
        }

        tv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
                startActivity(intent);


            }
        });
        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, FAQActivity.class);
                startActivity(intent);

            }
        });

        tv_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, MyOrderActivity.class);
                intent.putExtra("type", "place_order");
                startActivity(intent);

            }
        });

        tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, "Shop Your Needs is the best application to provide all the items of daily needs. It also provides many household services which will make our life easier. To download Shop Your Need app visit :- http://play.google.com/store/apps/details?id=com.mindscript.shopyouneeds");
                startActivity(Intent.createChooser(intent,"Share via"));
                finish();
            }
        });
        tv_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.mindscript.townmart"));
//                startActivity(intent);
            }
        });

        tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProfileActivity.this,AboutUsActivity.class);
                startActivity(intent);
                // finish();
            }
        });

        tv_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProfileActivity.this,ContactUsActivity.class);
                startActivity(intent);
                // finish();
            }
        });



        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreference.contains("u_id")) {
                    token = FirebaseInstanceId.getInstance().getToken();
                    unregistertoken(token);
                }





            }
        });

        String u_id= SharedPreference.get("u_id");
        viewProfile(u_id);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;
                case R.id.navigation_categories:
                    startActivity(new Intent(getApplicationContext(),CategoriesActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;
                case R.id.navigation_cart:
                    startActivity(new Intent(getApplicationContext(),MyCartActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;
                case R.id.navigation_person:


            }
            return true;

        }
    };
    private void viewProfile(final String u_id) {
        relativeLayout.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.VIEW_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik",response);
                relativeLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            f_name= jsonObject1.getString("u_name");
                            email=jsonObject1.getString("u_email");
                            city=jsonObject1.getString("u_city");
                            phone=jsonObject1.getString("u_phone");
                            address=jsonObject1.getString("u_address");
                            pin=jsonObject1.getString("u_pincode");
                        }
                        tv_name.setText(f_name);
                        tv_email.setText(email);
                        tv_phone.setText(phone);
                        tv_address.setText(address+", "+city+"- "+pin);


                    }else {
                        Toast.makeText(ProfileActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                relativeLayout.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_id",u_id);
                return params;
            }
        };
        AppController.getInstance().add(request);

    }
    private void unregistertoken(final String token) {
        StringRequest request=new StringRequest(Request.Method.POST,Keys.URL.UNREGISTER_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("vic","UnRegisterToken=>"+response);
                try{

                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        Intent intent=new Intent(ProfileActivity.this,LoginActivity.class);
                        GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(getApplicationContext(),gso);
                        googleSignInClient.signOut();
                        SharedPreference.removeKey("u_id");
                        SharedPreference.removeKey("f_token");
                        startActivity(intent);
                        finish();


                    }else {

                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("f_token",token);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        finish();


    }
}