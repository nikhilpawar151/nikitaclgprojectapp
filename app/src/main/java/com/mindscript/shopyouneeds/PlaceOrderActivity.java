package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mindscript.pojo.CartItemone;
import com.mindscript.util.AppController;
import com.mindscript.util.CartModel;
import com.mindscript.util.Keys;
import com.mindscript.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class
PlaceOrderActivity extends AppCompatActivity {

    ImageView back_img;
    Button button_submit;
    TextView tv_total,txt_delivery_charge,tv_total_price;
    ArrayList<CartItemone> list;
    String u_id="",name,address,phone,pin;
    String id,name1,p_quantity,photo,price,quantity,un_id,p_dis_type,p_discount,un_name,p_un_id_carton,o_payment,p_single_rate,p_un_name;
    Spinner sp_payment;
    int p_available,p_count_in_carton,p_available_carton;
    ArrayList<String> paymentList;
    EditText edtPlaceOrderNotes,edtName,edtPhone,edtAddress,edtPincode;
    int total=0;
    String u_type;
    int total_price;
    RelativeLayout place_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        paymentList=new ArrayList<>();
        paymentList.add(0,"Select Payment Type");
        paymentList.add("Cash");
        place_order=findViewById(R.id.progress_login);
        sp_payment=findViewById(R.id.spinner_payment);
        back_img=findViewById(R.id.back_btn);
        edtName=findViewById(R.id.edt_POname);
        edtPhone=findViewById(R.id.edt_POphone);
        edtAddress=findViewById(R.id.edt_POaddress);
        edtPincode=findViewById(R.id.edt_POpin);
        tv_total=findViewById(R.id.txt_total);
        tv_total_price=findViewById(R.id.txt_total_price);
        txt_delivery_charge=findViewById(R.id.txt_delivery_charge);
        button_submit=findViewById(R.id.btn_submit);
        u_id= SharedPreference.get("u_id");
        u_type=SharedPreference.get("u_type");
        list=new ArrayList<>();
        CartModel.getInstance(getApplicationContext());
        CartModel.open();

        // Cursor cursor = CartModel.database.rawQuery("select * from "+ Keys.CARTTABLE.TB_NAME,null);
        Cursor cursor = CartModel.database.rawQuery("select * from "+ Keys.CARTTABLE.TB_NAME,null);
        while(cursor.moveToNext()) {
//            id = cursor.getString(2);   //0 is the number of id column in your database table
//            name1 = cursor.getString(4);
//            price = cursor.getString(5);
//            p_quantity= cursor.getString(6);
//            un_id=cursor.getString(7);
//            photo=cursor.getString(8);
//            quantity = cursor.getString(10);
//            p_dis_type=cursor.getString(11);
//            p_discount=cursor.getColumnName(12);
            id = cursor.getString(2);   //0 is the number of id column in your database table
            name1 = cursor.getString(4);
            price = cursor.getString(5);
            p_dis_type=cursor.getString(6);
            p_discount=cursor.getColumnName(7);
            p_quantity= cursor.getString(8);
            un_id=cursor.getString(9);
            photo=cursor.getString(10);
            un_name=cursor.getString(11);
            quantity = cursor.getString(12);
            total +=Integer.parseInt(price)* Integer.parseInt(quantity);

            CartItemone cartItemone =new CartItemone(id,name1,price,p_dis_type,p_discount,p_quantity,un_id,photo,un_name,quantity);
            list.add(cartItemone);

        }
        tv_total.setText("₹ "+String.valueOf(total));
        if (total>300){
            txt_delivery_charge.setText("₹ 70");
            total_price=total+70;
        }else{
            txt_delivery_charge.setText("₹ 100");
            total_price=total+100;
        }
        tv_total_price.setText("₹ "+String.valueOf(total_price));
        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONArray jsArray = new JSONArray();

                for (int i=0;i<list.size();i++){
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("p_id",list.get(i).getId());
                        jsonObject.put("p_name",list.get(i).getP_name());
                        jsonObject.put("p_price",list.get(i).getP_price());
                        jsonObject.put("p_quantity",list.get(i).getU_quantity());
                        jsonObject.put("un_id",list.get(i).getUn_id());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsArray.put(jsonObject.toString());
                }
                if (total>300){
                    submit(jsArray);
                }else {
                    Toast.makeText(PlaceOrderActivity.this, "You can't placed the order below 300 Rs", Toast.LENGTH_SHORT).show();
                }


                //submit(jsArray);
            }
        });
        name= SharedPreference.get("u_name");
        address=SharedPreference.get("u_address");
        phone=SharedPreference.get("u_phone");
        pin=SharedPreference.get("u_pin");
        edtName.setText(name);
        edtPhone.setText(phone);
        edtAddress.setText(address);
        edtPincode.setText(pin);


        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,paymentList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_payment.setAdapter(adapter);
//
//        sp_payment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (parent.getItemAtPosition(position).equals("Select Payment Type")){
//                    //Toast.makeText(PlaceOrderActivity.this, "Please select payment type", Toast.LENGTH_SHORT).show();
//                }else {
//                    o_payment=parent.getItemAtPosition(position).toString();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                Toast.makeText(PlaceOrderActivity.this, "Please select payment type", Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public void onBackPressed() {

        finish();
    }
    private void submit(final JSONArray jsArray) {
        Log.i("pri",""+jsArray.toString());
        final String name1=edtName.getText().toString().trim();
        final String phone1=edtPhone.getText().toString().trim();
        final String address1=edtAddress.getText().toString().trim();
        final String pincode=edtPincode.getText().toString().trim();
        o_payment= sp_payment.getSelectedItem().toString();
        int pos =sp_payment.getSelectedItemPosition();
        if(pos!=0)
        {
            o_payment = sp_payment.getSelectedItem().toString();
        }else{
            Toast.makeText(PlaceOrderActivity.this, "Please Select payment type", Toast.LENGTH_LONG).show();
            return;
        }
        if(!o_payment.equals("Choose policy"))
        {
            o_payment = sp_payment.getSelectedItem().toString();
        }
        else{
            Toast.makeText(PlaceOrderActivity.this, "Please Select payment type", Toast.LENGTH_LONG).show();
            return;
        }

        if (name.equals("")||phone.equals("")|| address.equals("")||pincode.equals("")){
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
        }else {
            place_order.setVisibility(View.VISIBLE);
            StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.SUBMIT_ORDER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("nik", response);
                    place_order.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("success").equals("1")) {
                            CartModel.open();
                            Cursor cursor = CartModel.database.rawQuery("select * from " + Keys.CARTTABLE.TB_NAME, null);
                            while (cursor.moveToNext()) {
                                String pc_id = cursor.getString(0);   //0 is the number of id column in your database table
                                SQLiteDatabase db = CartModel.dbHelper.getWritableDatabase();
                                db.delete(Keys.CARTTABLE.TB_NAME, "pc_id = ?", new String[]{pc_id});
                                Log.i("nik", "value decrement updated");


                            }
                            CartModel.close();
                            String order_id=jsonObject.getString("order_id");
                            String o_no=jsonObject.getString("o_no");
                            String o_time=jsonObject.getString("o_time");
                            String o_total=jsonObject.getString("o_total");
                            String o_status=jsonObject.getString("o_status");
                            Intent intent = new Intent(PlaceOrderActivity.this,OrderDetailsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("o_id",order_id);
                            intent.putExtra("o_no",o_no);
                            intent.putExtra("o_time",o_time);
                            intent.putExtra("o_total",o_total);
                            intent.putExtra("o_status",o_status);
                            intent.putExtra("type","place_order");
                            startActivity(intent);
                            finish();
                            Toast.makeText(PlaceOrderActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(PlaceOrderActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(PlaceOrderActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    place_order.setVisibility(View.GONE);
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("cart", jsArray.toString());
                    params.put("o_name", name1);
//                    params.put("o_note", edtPlaceOrderNotes.getText().toString());
                    params.put("o_total", total + "");
                    params.put("o_phone", phone1);
                    params.put("o_address", address1);
                    params.put("o_email", SharedPreference.get("u_email"));
                    params.put("o_pin", pincode);
                    if (o_payment.equals("Cash")){
                        params.put("o_payment","1");
                    }
                    if (total>300){
                        params.put("o_delivery","70");
                    }else{
                        params.put("o_delivery","100");
                    }
                    params.put("u_id", SharedPreference.get("u_id"));
                    return params;
                }
            };
            AppController.getInstance().add(request);
        }

    }
}
