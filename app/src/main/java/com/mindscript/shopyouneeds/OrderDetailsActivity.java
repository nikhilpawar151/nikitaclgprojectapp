package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mindscript.adapter.OrderDetailsAdapter;
import com.mindscript.pojo.OrderDetails;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderDetailsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<OrderDetails> list;
    ImageView img_back;
    ProgressBar pBarODetails;
    TextView tvONo, tvODate, tvOTotal,tvStatus,tvTitle,tv_name,tv_phone;
    ImageView user_img;
    LinearLayout layout_volunteer;
    TextView tv_invoice;

    String o_id, o_time, o_no, o_total, type,o_status_type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        Intent i = getIntent();
        o_id = i.getStringExtra("o_id");
        o_time = i.getStringExtra("o_time");
        o_no = i.getStringExtra("o_no");
        o_total = i.getStringExtra("o_total");
        o_status_type = i.getStringExtra("o_status");
        type = i.getStringExtra("type");
        list=new ArrayList<>();
        recyclerView=findViewById(R.id.recycler_order_details);
        layout_volunteer=findViewById(R.id.volunteer_details);
        pBarODetails = findViewById(R.id.progressBarOrderDetails);
        tvONo = findViewById(R.id.o_no);
        tvODate = findViewById(R.id.o_date);
        tvOTotal = findViewById(R.id.textViewOTotal);
        tvStatus = findViewById(R.id.oo_status);
        tvTitle = findViewById(R.id.orderdetailstitle);
        user_img=findViewById(R.id.imgProfilePic);
        tv_name=findViewById(R.id.txt_name);
        tv_phone=findViewById(R.id.txt_phone);
        tv_invoice=findViewById(R.id.txt_invoice);
        //tvONo.setText("Order No: " + o_no);
        tvTitle.setText("Order "+o_no);
        tvODate.setText("Order Time: " + o_time);
        String o_status="";
        if (o_status_type.equals("0")){
            o_status="Order Submitted";
        }else if (o_status_type.equals("1")){
            getVolunteerDetails(o_id);
            layout_volunteer.setVisibility(View.VISIBLE);
            o_status="Order Accepted";
        }else if (o_status_type.equals("2")){
            o_status="Order Rejected";
        }else if (o_status_type.equals("3")){
            o_status="Order On The Way";
            getVolunteerDetails(o_id);
            layout_volunteer.setVisibility(View.VISIBLE);
        }else if (o_status_type.equals("4")){
            getVolunteerDetails(o_id);
            layout_volunteer.setVisibility(View.VISIBLE);
            o_status="Order Delivered";
            tv_invoice.setVisibility(View.VISIBLE);
            tv_invoice.setText("Print Invoice");
        }
        tvStatus.setText(o_status);


        tv_invoice.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        tv_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://shopyourneeds.xyz/admin/invoice.php?o_id="+o_id;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });
        LinearLayoutManager layoutManager1=new LinearLayoutManager(OrderDetailsActivity.this);
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setHasFixedSize(true);

        getOrderDetails(o_id);
      //  getVolunteerDetails(o_id);


        img_back=findViewById(R.id.back_btn);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("place_order")){
                    Intent intent = new Intent(OrderDetailsActivity.this, MyOrderActivity.class);
                    intent.putExtra("type","place_order");
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }

            }
        });
    }

    private void getVolunteerDetails(String o_id) {
        pBarODetails.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.VOLUNTEER_DETAILS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pBarODetails.setVisibility(View.GONE);
                Loggers.i(response);
                try {
                    JSONObject json = new JSONObject(response);
                    if(json.getString("success").equals("1")){
                        JSONArray jA = json.getJSONArray("data");
                        for(int i = 0; i < jA.length(); i++){
                            JSONObject j = jA.getJSONObject(i);
                            String v_id=j.getString("v_id");
                            String v_name=j.getString("v_name");
                            String v_phone=j.getString("v_phone");
                            String v_photo=j.getString("v_photo");
                            tv_name.setText(v_name);
                            tv_phone.setText(v_phone);
                            Picasso.with(OrderDetailsActivity.this)
                                    .load(Keys.VOLUNTEER_PATH + v_photo)
                                    .into(user_img);
                            layout_volunteer.setVisibility(View.VISIBLE);
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), json.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBarODetails.setVisibility(View.GONE);
                error.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("o_id", o_id);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }

    private void getOrderDetails(final String o_id) {
        pBarODetails.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.ORDER_DETAILS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pBarODetails.setVisibility(View.GONE);
                Loggers.i(response);
                try {
                    JSONObject json = new JSONObject(response);
                    if(json.getString("success").equals("1")){
                        JSONArray jA = json.getJSONArray("data");
                        for(int i = 0; i < jA.length(); i++){
                            JSONObject j = jA.getJSONObject(i);
                            OrderDetails od = new OrderDetails(j.getInt("od_id"), j.getInt("o_id"), j.getInt("p_id"),
                                    j.getInt("p_quantity"),j.getInt("un_id"), j.getString("p_name"),
                                    j.getString("p_rate"), j.getString("un_name"), j.getString("p_photo"));
                            list.add(od);
                        }
                        tvOTotal.setText("Total Price (" + list.size() + " items) : " +" ₹ "+ o_total + ".");
                        OrderDetailsAdapter orderAdapter=new OrderDetailsAdapter(OrderDetailsActivity.this,list);
                        recyclerView.setAdapter(orderAdapter);
                    }else{
                        Toast.makeText(getApplicationContext(), json.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBarODetails.setVisibility(View.GONE);
                error.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("o_id", o_id);
                return params;
            }
        };

        AppController.getInstance().add(request);
    }

    @Override
    public void onBackPressed() {
        if (type.equals("place_order")){
            Intent intent = new Intent(OrderDetailsActivity.this, MyOrderActivity.class);
            intent.putExtra("type","place_order");
            startActivity(intent);
            finish();
        }else {
            finish();
        }
    }
}