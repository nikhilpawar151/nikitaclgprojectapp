package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;
import com.mindscript.util.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ForgotPasswordActivity extends AppCompatActivity {

    EditText editText_phone;
    Button button_forgot;
    String u_phone,u_email;
    RelativeLayout progreess;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        editText_phone=findViewById(R.id.edt_phone);
        button_forgot=findViewById(R.id.btn_forgot_password);
        progreess=findViewById(R.id.progress_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            Drawable background = getResources().getDrawable(R.drawable.background); //bg_gradient is your gradient.
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }

        button_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                u_email=editText_phone.getText().toString().trim();
                if (u_email.equals("")){
                    Toast.makeText(ForgotPasswordActivity.this, "Please fill all the details", Toast.LENGTH_SHORT).show();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(u_email).matches()){
                    editText_phone.setError("Invalid Email ID");
                }
//                else if (u_phone.length()!=10){
//                    editText_phone.setError("Phone number must be 10 digits");
//                }
                else {
                    sendEmail(u_email);
                    //Otp();
                }
            }
        });


    }

    private void sendEmail(String u_email) {
        progreess.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progreess.setVisibility(View.GONE);
                Log.i("pri","Forgot pass =>>"+response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        //JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        Intent intent=new Intent(ForgotPasswordActivity.this,ChangepasswordActivity.class);
                        intent.putExtra("u_email",u_email);
                        startActivity(intent);
                        editText_phone.setText("");
                        finish();
                    }else {
                        Toast.makeText(ForgotPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ForgotPasswordActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreess.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_email",u_email);

                return params;
            }
        };
        AppController.getInstance().add(request);
    }


    private void sendOTP(String u_phone, String otp) {

        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("pri","Forgot pass =>>"+response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        //JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        Intent intent=new Intent(ForgotPasswordActivity.this,ChangepasswordActivity.class);
                        intent.putExtra("phone",u_phone);
                        startActivity(intent);
                        editText_phone.setText("");
                        finish();
                    }else {
                        Toast.makeText(ForgotPasswordActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ForgotPasswordActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // relativeLayout.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("u_email",u_phone);
               // params.put("u_otp",otp);
                Log.i("pri","email pas==>"+u_phone +"   "+otp);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }



}