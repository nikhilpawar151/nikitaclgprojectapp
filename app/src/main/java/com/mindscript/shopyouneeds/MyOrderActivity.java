package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mindscript.adapter.MyCartAdapter;
import com.mindscript.adapter.MyOrderAdapter;
import com.mindscript.pojo.CartItemone;
import com.mindscript.pojo.OrderList;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyOrderActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    ArrayList<OrderList> list;
    ImageView imageView;
    ProgressBar pBarMyOrder;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        list=new ArrayList<>();
        recyclerView=findViewById(R.id.recycler_my_orders);
        pBarMyOrder = findViewById(R.id.progressBarMyOrder);
        imageView=findViewById(R.id.orderback_btn);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("place_order")){
                    Intent intent = new Intent(MyOrderActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }
            }
        });
        Intent i = getIntent();
        type = i.getStringExtra("type");
        loadMyOrders();
        LinearLayoutManager layoutManager1=new LinearLayoutManager(MyOrderActivity.this);
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView.setHasFixedSize(true);
    }

    private void loadMyOrders() {
        pBarMyOrder.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.POST, Keys.URL.MY_ORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);
                pBarMyOrder.setVisibility(View.GONE);

                try {
                    JSONObject json = new JSONObject(response);
                    if(json.getString("success").equals("1")){
                        JSONArray jA = json.getJSONArray("data");
                        for(int i = 0; i < jA.length(); i++){
                            JSONObject j = jA.getJSONObject(i);
                            OrderList ol = new OrderList(j.getString("o_id"), j.getString("o_no"), j.getString("o_time"),
                                    j.getString("o_name"), j.getString("o_phone"), j.getString("o_address"),
                                    j.getString("o_pin"), j.getString("o_email"),
                                    j.getString("o_total"), j.getString("o_status"));
                            list.add(ol);
                        }
                        MyOrderAdapter orderAdapter=new MyOrderAdapter(MyOrderActivity.this,list);
                        recyclerView.setAdapter(orderAdapter);
                    }else{
                        Toast.makeText(getApplicationContext(), json.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pBarMyOrder.setVisibility(View.GONE);
                error.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("u_id", SharedPreference.get("u_id"));
                return params;
            }
        };

        AppController.getInstance().add(request);

    }

    @Override
    public void onBackPressed() {
        if (type.equals("place_order")){
            Intent intent = new Intent(MyOrderActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }else {
            finish();
        }

    }
}