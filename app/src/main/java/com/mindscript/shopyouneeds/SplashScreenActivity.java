package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.mindscript.util.AppController;
import com.mindscript.util.SharedPreference;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        FirebaseAnalytics.getInstance(getApplicationContext());
//        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        AppController.initialize(getApplicationContext());
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPreference.contains("u_id") && SharedPreference.contains("u_email")) {
                    Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(SplashScreenActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },3000);
    }
}