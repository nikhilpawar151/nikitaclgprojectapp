package com.mindscript.shopyouneeds;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

public class AboutUsActivity extends AppCompatActivity {

    WebView webView;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        webView=findViewById(R.id.about_web);
        img_back=findViewById(R.id.back_btn);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(AboutUsActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadData("<html> <body><p>Here everything you can do and find on the Shop Your Need App.<br>\n" +
                        "-Order groceries and other essentials online.<br>\n" +
                        "-Deliveries are being done with hygienic care & handled carefully.<br>\n" +
                        "-We are prioritily delivering Kirana goods such as:<br>\n" +
                        "Rice, Atta, spices, breakfast eating products.<br>\n" +
                        "<p>Safety Measures<br>\n" +
                        "The safety of everyone involved in the delivery chain is our priority<br>\n" +
                        "- Delivery Partners trained in the best hygiene protocol and have been equipped with masks.<br>\n" +
                        "- Groceries/service Partners are frequently updated on necessary sanitization measures.</p>\n" +
                        "\n" +
                        "<p>Payments<br>\n" +
                        "Initially, we are doing deliveries by pay on delivery basis, so that customer can directly interact with us<br>\n" +
                        "regarding the quality & hygiene of goods.</p>\n" +
                        "</body> </html>",
                "text/html", "UTF-8");

    }
    @Override
    public void onBackPressed() {
        Intent intent=new Intent(AboutUsActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
    }
}