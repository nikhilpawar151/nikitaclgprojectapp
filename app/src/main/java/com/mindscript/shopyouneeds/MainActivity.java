package com.mindscript.shopyouneeds;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mindscript.adapter.AllProductAdapter;
import com.mindscript.adapter.ProductAdapter;
import com.mindscript.adapter.ViewPageAdapter;
import com.mindscript.categoryclick;
import com.mindscript.pojo.BannerList;
import com.mindscript.pojo.HomeCategoryList;
import com.mindscript.util.AppController;
import com.mindscript.util.CartModel;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    ViewPageAdapter viewPageAdapter;
    ArrayList<BannerList> list;
    RecyclerView recyclerview_category;
    ProgressBar progressBar_category;
    LinearLayout sliderdot;
    private int dotcount;
    ImageView[] dots;
    Timer timer;
    RelativeLayout header;
    EditText editText_search;
    DrawerLayout drawer;
    private AppBarLayout appBarLayout;
    NavigationView navigationView;
    ImageView imageViewMenu,viewCart;
    private CollapsingToolbarLayout collapsingToolbar;
    ArrayList<HomeCategoryList> categoryList;
    TextView tvCartCount;
    ProductAdapter adapter;
    ProductAdapter productAdapter;
    String token;
    String value="0";
    BottomNavigationView bottomNavigationView;
    TextView tv_title;
    ShimmerFrameLayout shimmerFrameLayout;
    ImageView img_category_first,img_category_second,img_category_third,img_category_four,img_category_all;
    TextView tv_category_first,tv_category_second,tv_category_third,tv_category_four,tv_category_all;
    GoogleSignInOptions gso;





    TextView tv_order,tv_profile,tv_about,tv_contact,tvUsername,tv_logout,tv_share,tv_rate,tv_faq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppController.initialize(getApplicationContext());
        SharedPreference.initialize(getApplicationContext());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        gso = new GoogleSignInOptions.
                Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                build();
        header=findViewById(R.id.header);
        tv_title=findViewById(R.id.txt_title_head);
        list=new ArrayList<>();
        categoryList=new ArrayList<>();
        recyclerview_category=findViewById(R.id.recycler_category);
        appBarLayout=findViewById(R.id.appbar);
        viewPager=findViewById(R.id.viewPager);
        sliderdot=findViewById(R.id.slider_dots);
        editText_search=findViewById(R.id.edt_search);
        shimmerFrameLayout=findViewById(R.id.shimmerFrameLayout);
        shimmerFrameLayout.startShimmerAnimation();
        shimmerFrameLayout.stopShimmerAnimation();


        img_category_first=findViewById(R.id.cat_img_one);
        img_category_second=findViewById(R.id.cat_img_two);
        img_category_third=findViewById(R.id.cat_img_three);
        img_category_four=findViewById(R.id.cat_img_four);
        img_category_all=findViewById(R.id.cat_img_all);
        tv_category_first=findViewById(R.id.cat_txt_one);
        tv_category_second=findViewById(R.id.cat_txt_two);
        tv_category_third=findViewById(R.id.cat_txt_three);
        tv_category_four=findViewById(R.id.cat_txt_four);
        tv_category_all=findViewById(R.id.cat_txt_all);

        editText_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, ProductActivity.class);
                intent.putExtra("search",value);
                startActivity(intent);
            }
        });
        LinearLayoutManager layoutManager=new LinearLayoutManager(MainActivity.this);
        recyclerview_category.setLayoutManager(layoutManager);
        recyclerview_category.setHasFixedSize(true);
        category();
//        list=new ArrayList<>();
//        categoryList=new ArrayList<>();
//        recyclerview_category=findViewById(R.id.recycler_category);
//        relativeLayout_header=findViewById(R.id.header);
//        appBarLayout=findViewById(R.id.appbar);
//        viewPager=findViewById(R.id.viewPager);
//        sliderdot=findViewById(R.id.slider_dots);
//        progressBar_banner=findViewById(R.id.progress_banner);
//        progressBar_category=findViewById(R.id.progress_category_list);
//        editText_search=findViewById(R.id.edt_search);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        imageViewMenu = findViewById(R.id.menu_button);
        tvCartCount=findViewById(R.id.textViewCartCount);
        viewCart=findViewById(R.id.view_cart);
        //loadFragment(new HomeFragment());
        viewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,MyCartActivity.class);
                startActivity(intent);
            }
        });
        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(ProfileActivity.this, "grtvgte", Toast.LENGTH_SHORT).show();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
//            @Override
//            public void onNavigationItemReselected(@NonNull MenuItem item) {
//                switch (item.getItemId()){
//                    case R.id.navigation_home:
//
//                    case R.id.navigation_categories:
//                        startActivity(new Intent(getApplicationContext(),CategoriesActivity.class));
//                        finish();
//                        overridePendingTransition(0,0);
//                        return;
//                    case R.id.navigation_cart:
//                        startActivity(new Intent(getApplicationContext(),MyCartActivity.class));
//                        finish();
//                        overridePendingTransition(0,0);
//                        return;
//                    case R.id.navigation_person:
//                        startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
//                        finish();
//                        overridePendingTransition(0,0);
//                        return;
//
//                }
//            }
//        });

        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.VIEW_BANNERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Loggers.i(response);


                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            list.add(new BannerList(jsonObject1.getString("b_id"),
                                    jsonObject1.getString("b_image"),
                                    jsonObject1.getString("b_status")));
                        }

                        viewPageAdapter=new ViewPageAdapter(MainActivity.this,list);
                        viewPager.setAdapter(viewPageAdapter);
                        dotcount=viewPageAdapter.getCount();

                        dots=new ImageView[dotcount];
                        for (int i=0;i<dotcount;i++){
                            dots[i]=new ImageView(MainActivity.this);
                            dots[i].setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.nonactive_dot));
                            LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(4,0,4,0);
                            sliderdot.addView(dots[i],params);
                        }
                        dots[0].setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.active_dot));
                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                for (int i=0;i<dotcount;i++){
                                    dots[i].setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.nonactive_dot));
                                }
                                dots[position].setImageDrawable(ContextCompat.getDrawable(MainActivity.this,R.drawable.active_dot));



                            }

                            @Override
                            public void onPageSelected(int position) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });

                        startViewPagerTimer();

                    }else {
                        Toast.makeText(MainActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };
        AppController.getInstance().add(request);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View headerLayout = navigationView.getHeaderView(0);
        tvUsername=headerLayout.findViewById(R.id.textViewNavigateMenuUsername);
        tv_profile=headerLayout.findViewById(R.id.txt_profile);
        tv_order=headerLayout.findViewById(R.id.txt_orders);
        tv_about=headerLayout.findViewById(R.id.txt_about);
        tv_contact=headerLayout.findViewById(R.id.txt_contact);
        tv_logout=headerLayout.findViewById(R.id.txt_logout);
        tv_share=headerLayout.findViewById(R.id.txt_shareapp);
        tv_rate=headerLayout.findViewById(R.id.txt_rateus);
        tv_faq=headerLayout.findViewById(R.id.txt_faq);

        if (SharedPreference.contains("u_id")) {
            tvUsername.setText(SharedPreference.get("u_name"));
        }

        tv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);


            }
        });
        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FAQActivity.class);
                startActivity(intent);


            }
        });

        tv_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MyOrderActivity.class);
                intent.putExtra("type", "place_order");
                startActivity(intent);

            }
        });

        tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, "Shop Your Needs is the best application to provide all the items of daily needs. It also provides many household services which will make our life easier. To download Shop Your Need app visit :- http://play.google.com/store/apps/details?id=com.mindscript.shopyouneeds");
                startActivity(Intent.createChooser(intent,"Share via"));
                 finish();
            }
        });
        tv_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.mindscript.townmart"));
//                startActivity(intent);
            }
        });

        tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,AboutUsActivity.class);
                startActivity(intent);
                // finish();
            }
        });

        tv_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,ContactUsActivity.class);
                startActivity(intent);
                // finish();
            }
        });

        showcartcount();
        AllProductAdapter.bindListener(new categoryclick() {
            @Override
            public void click(String c_id) {
                showcartcount();
            }
        });
        ProductAdapter.bindListener(new categoryclick() {
            @Override
            public void click(String c_id) {

                showcartcount();
            }
        });
        GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(getApplicationContext(),gso);

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreference.contains("u_id")) {
                    token = FirebaseInstanceId.getInstance().getToken();
                    SharedPreference.removeKey("u_id");
                    SharedPreference.removeKey("f_token");
                    Intent intent=new Intent(MainActivity.this,LoginActivity.class);
                    googleSignInClient.signOut();
                    startActivity(intent);
                    finish();
                    unregistertoken(token);
                }





            }
        });

        if (SharedPreference.contains("u_id")) {
            if (SharedPreference.contains("f_token")) {
                // registertoken(SharedPreference.get("t_ipl_id"));
            } else {
                // token = FirebaseInstanceId.getInstance().getToken();
                //registertoken(token);
            }
        }
        token = FirebaseInstanceId.getInstance().getToken();
        registertoken(token, SharedPreference.get("u_id"));

    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:

                case R.id.navigation_categories:
                    startActivity(new Intent(getApplicationContext(),CategoriesActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;
                case R.id.navigation_cart:
                    startActivity(new Intent(getApplicationContext(),MyCartActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;
                case R.id.navigation_person:
                    startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;

            }
            return true;

        }
    };

//    private void loadFragment(Fragment fragment) {
//        // load fragment
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame_container, fragment);
//        transaction.commit();
//    }

    private void services() {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.VIEW_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik",response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

//                        JSONArray jsonArray=jsonObject.getJSONArray("data");
//                        for (int i=0;i<jsonArray.length();i++)
//                        {
//                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
//                            serviceLists.add(new ServiceList(
//                                    jsonObject1.getString("s_id"),
//                                    jsonObject1.getString("s_name"),
//                                    jsonObject1.getString("s_description"),
//                                    jsonObject1.getString("s_rate"),
//                                    jsonObject1.getString("s_photo"),
//                                    jsonObject1.getString("s_enable")));
//                        }
//                        serviceHomeAdapter=new ServiceHomeAdapter(MainActivity.this,serviceLists);
//                        recyclerView_services.setAdapter(serviceHomeAdapter);


                    }else {

                        Toast.makeText(MainActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();

                return params;
            }
        };
        AppController.getInstance().add(request);

    }

    private void registertoken(String token, final String u_id) {
        StringRequest request=new StringRequest(Request.Method.POST,Keys.URL.REGISTER_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("vic","RegisterToken=>"+response);
                try{

                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        SharedPreference.save("f_token",token);
                    }else {

                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("f_token",token);
                params.put("u_id",SharedPreference.get("u_id"));
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void unregistertoken(final String token) {
        StringRequest request=new StringRequest(Request.Method.POST,Keys.URL.UNREGISTER_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("vic","UnRegisterToken=>"+response);
                try{

                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        Toast.makeText(MainActivity.this, "Logout Successfully.", Toast.LENGTH_SHORT).show();
                    }else {

                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("f_token",token);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }

    private void allProduct() {
//        productList.clear();
//        progressBar_product.setVisibility(View.VISIBLE);
//        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.ALL_PRODUCT_LIST, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.i("nik",response);
//                progressBar_product.setVisibility(View.GONE);
//
//                try {
//                    JSONObject jsonObject=new JSONObject(response);
//                    if (jsonObject.getString("success").equals("1")){
//
//                        JSONArray jsonArray=jsonObject.getJSONArray("data");
//                        for (int i=0;i<jsonArray.length();i++)
//                        {
//                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
//                            productList.add(new AllProduct(
//                                    jsonObject1.getString("p_id"),
//                                    jsonObject1.getString("c_id"),
//                                    jsonObject1.getString("p_name"),
//                                    jsonObject1.getString("p_description"),
//                                    jsonObject1.getString("p_price"),
//                                    jsonObject1.getString("p_quantity"),
//                                    jsonObject1.getString("un_id"),
//                                    jsonObject1.getString("p_photo"),
//                                    jsonObject1.getString("p_enable"),
//                                    jsonObject1.getString("p_stock"),
//                                    jsonObject1.getString("un_name")));
//                        }
//
//                        adapter=new AllProductAdapter(MainActivity.this,productList);
//                        recyclerView_product.setAdapter(adapter);
//
//                    }else {
//                        Toast.makeText(MainActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(MainActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressBar_product.setVisibility(View.GONE);
//                error.printStackTrace();
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> params=new HashMap<>();
//                return params;
//            }
//        };
//        AppController.getInstance().add(request);
    }

    private void showcartcount() {
        CartModel.getInstance(getApplicationContext());
        CartModel.open();
        String u_id=SharedPreference.get("u_id");
        String sql = "select * from "+ Keys.CARTTABLE.TB_NAME;
        Loggers.i(sql);
        Cursor cursor = CartModel.database.rawQuery(sql ,null);
        ArrayList<String> alpc_id=new ArrayList<>();
        if(cursor != null) {
            Loggers.i("======================= " + cursor.getCount());
            while (cursor.moveToNext()) {
                alpc_id.add(cursor.getString(0));
            }
            tvCartCount.setText(alpc_id.size()+"");
        }
        CartModel.close();
    }

//    private void category() {
//        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CATEGORY_LIST, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.i("nik",response);
//
//
//                try {
//                    JSONObject jsonObject=new JSONObject(response);
//                    if (jsonObject.getString("success").equals("1")){
//
//                        JSONArray jsonArray=jsonObject.getJSONArray("data");
//                        for (int i=0;i<jsonArray.length();i++)
//                        {
//                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
//                            categoryList.add(new CategoryList(
//                                    jsonObject1.getString("c_id"),
//                                    jsonObject1.getString("c_name"),
//                                    jsonObject1.getString("c_photo"),
//                                    jsonObject1.getString("c_enable")));
//                        }
////                        if (!categoryList.isEmpty()) {
////                            allproductList(categoryList.get(0).getC_id());
////                        }
//
//                        CategoryListAdapter categoryAdapter=new CategoryListAdapter(MainActivity.this,categoryList);
//                        recyclerview_category.setAdapter(categoryAdapter);
//
//                    }else {
//                        Toast.makeText(MainActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(MainActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> params=new HashMap<>();
//                return params;
//            }
//        };
//        AppController.getInstance().add(request);
//
//
//
//    }

    @Override
    protected void onStart() {
        super.onStart();
        if (adapter!=null){
            adapter.notifyDataSetChanged();
            showcartcount();
        }
    }

    private void category() {
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("nik",response);


                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){

                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            categoryList.add(new HomeCategoryList(
                                    jsonObject1.getString("c_id"),
                                    jsonObject1.getString("c_name"),
                                    jsonObject1.getString("c_photo"),
                                    jsonObject1.getString("c_enable")));
                        }

                        Picasso.with(MainActivity.this).load(Keys.CATEGORY_PATH+categoryList.get(0).getC_photo()).into(img_category_first);
                        Picasso.with(MainActivity.this).load(Keys.CATEGORY_PATH+categoryList.get(1).getC_photo()).into(img_category_second);
                        Picasso.with(MainActivity.this).load(Keys.CATEGORY_PATH+categoryList.get(2).getC_photo()).into(img_category_third);
                        Picasso.with(MainActivity.this).load(Keys.CATEGORY_PATH+categoryList.get(3).getC_photo()).into(img_category_four);
                        img_category_all.setImageResource(R.drawable.categoryicon);
                        img_category_all.setPadding(7,7,7,7);
                        tv_category_first.setText(categoryList.get(0).getC_name());
                        tv_category_second.setText(categoryList.get(1).getC_name());
                        tv_category_third.setText(categoryList.get(2).getC_name());
                        tv_category_four.setText(categoryList.get(3).getC_name());
                        tv_category_first.setBackgroundColor(getResources().getColor(R.color.transparent));
                        tv_category_second.setBackgroundColor(getResources().getColor(R.color.transparent));
                        tv_category_third.setBackgroundColor(getResources().getColor(R.color.transparent));
                        tv_category_four.setBackgroundColor(getResources().getColor(R.color.transparent));
                        tv_category_all.setBackgroundColor(getResources().getColor(R.color.transparent));


                        img_category_second.setBackgroundColor(getResources().getColor(R.color.black));
                        img_category_all.setBackgroundColor(getResources().getColor(R.color.purple_200));
                        img_category_all.setColorFilter(getResources().getColor(R.color.white));
                        tv_category_all.setText("See All");

                        img_category_first.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(MainActivity.this,ProductActivity.class);
                                intent.putExtra("search",categoryList.get(0).getC_id());
                                intent.putExtra("c_name",categoryList.get(0).getC_name());
                                startActivity(intent);

                            }
                        });


                        img_category_second.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(MainActivity.this,ProductActivity.class);
                                intent.putExtra("search",categoryList.get(1).getC_id());
                                intent.putExtra("c_name",categoryList.get(1).getC_name());
                                startActivity(intent);

                            }
                        });

                        img_category_third.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(MainActivity.this,ProductActivity.class);
                                intent.putExtra("search",categoryList.get(2).getC_id());
                                intent.putExtra("c_name",categoryList.get(2).getC_name());
                                startActivity(intent);

                            }
                        });

                        img_category_four.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(MainActivity.this,ProductActivity.class);
                                intent.putExtra("search",categoryList.get(3).getC_id());
                                intent.putExtra("c_name",categoryList.get(3).getC_name());
                                startActivity(intent);

                            }
                        });

                        img_category_all.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent=new Intent(MainActivity.this,CategoriesActivity.class);
                                startActivity(intent);
                                finish();
//                                Fragment fragment=new CategoryFragment();
//                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                                transaction.replace(R.id.frame_container, fragment);
//                                transaction.commit();
                            }
                        });

//                        if (!categoryList.isEmpty()) {
//                            allproductList(categoryList.get(0).getC_id());
//                        }

//                        CategoryAdapter categoryAdapter=new CategoryAdapter(MainActivity.this,categoryList);
//                        recyclerview_category.setAdapter(categoryAdapter);

                    }else {
                        Toast.makeText(MainActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };
        AppController.getInstance().add(request);



    }


    private void startViewPagerTimer() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                viewPager.post(new Runnable(){

                    @Override
                    public void run() {
                        viewPager.setCurrentItem((viewPager.getCurrentItem()+1)%dots.length);
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, 2000, 6000);
    }

}
