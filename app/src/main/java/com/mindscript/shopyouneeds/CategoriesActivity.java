package com.mindscript.shopyouneeds;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mindscript.adapter.CategoryListAdapter;
import com.mindscript.pojo.CategoryList;
import com.mindscript.pojo.ProductNameList;
import com.mindscript.util.AppController;
import com.mindscript.util.Keys;
import com.mindscript.util.Loggers;
import com.mindscript.util.SharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CategoriesActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    RelativeLayout header;
    ImageView imageViewMenu;
    DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout progress_list;
    RecyclerView recyclerView_category;
    ArrayList<CategoryList> categoryList=new ArrayList<>();
    CategoryListAdapter adapter;
    ShimmerFrameLayout shimmerFrameLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    GoogleSignInOptions gso;
    String token;
    TextView tv_order,tv_profile,tv_about,tv_contact,tvUsername,tv_logout,tv_share,tv_rate,tv_faq;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        gso = new GoogleSignInOptions.
                Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                build();
        AppController.initialize(getApplicationContext());
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        header=findViewById(R.id.header);
        imageViewMenu = findViewById(R.id.menu_button);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        progress_list=findViewById(R.id.progress_category_list);
        recyclerView_category=findViewById(R.id.recycler_category);
        LinearLayoutManager layoutManager=new LinearLayoutManager(CategoriesActivity.this);
        recyclerView_category.setLayoutManager(layoutManager);
        recyclerView_category.setHasFixedSize(true);
        shimmerFrameLayout=findViewById(R.id.shimmerFrameLayout);
        swipeRefreshLayout=findViewById(R.id.sw_data);
        shimmerFrameLayout.startShimmerAnimation();
        shimmerFrameLayout.stopShimmerAnimation();
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.purple_200));
        category();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                categoryList.clear();
                category();


            }
        });

          View headerLayout = navigationView.getHeaderView(0);
        tvUsername=headerLayout.findViewById(R.id.textViewNavigateMenuUsername);
        tv_profile=headerLayout.findViewById(R.id.txt_profile);
        tv_order=headerLayout.findViewById(R.id.txt_orders);
        tv_about=headerLayout.findViewById(R.id.txt_about);
        tv_contact=headerLayout.findViewById(R.id.txt_contact);
        tv_logout=headerLayout.findViewById(R.id.txt_logout);
        tv_share=headerLayout.findViewById(R.id.txt_shareapp);
        tv_rate=headerLayout.findViewById(R.id.txt_rateus);
        tv_faq=headerLayout.findViewById(R.id.txt_faq);

        if (SharedPreference.contains("u_id")) {
            tvUsername.setText(SharedPreference.get("u_name"));
        }

        tv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoriesActivity.this, ProfileActivity.class);
                startActivity(intent);


            }
        });
        tv_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoriesActivity.this, FAQActivity.class);
                startActivity(intent);

            }
        });

        tv_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CategoriesActivity.this, MyOrderActivity.class);
                intent.putExtra("type", "place_order");
                startActivity(intent);

            }
        });

        tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, "Shop Your Needs is the best application to provide all the items of daily needs. It also provides many household services which will make our life easier. To download Shop Your Need app visit :- http://play.google.com/store/apps/details?id=com.mindscript.shopyouneeds");
                startActivity(Intent.createChooser(intent,"Share via"));
                finish();
            }
        });
        tv_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.mindscript.townmart"));
//                startActivity(intent);
            }
        });

        tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CategoriesActivity.this,AboutUsActivity.class);
                startActivity(intent);
                // finish();
            }
        });

        tv_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CategoriesActivity.this,ContactUsActivity.class);
                startActivity(intent);
                // finish();
            }
        });



        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreference.contains("u_id")) {
                    token = FirebaseInstanceId.getInstance().getToken();
                    unregistertoken(token);
                }





            }
        });
//        adapter=new CategoryListAdapter(CategoriesActivity.this,categoryList);
//        recyclerView_category.setAdapter(adapter);

        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(ProfileActivity.this, "grtvgte", Toast.LENGTH_SHORT).show();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.navigation_categories);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
//            @Override
//            public void onNavigationItemReselected(@NonNull MenuItem item) {
//                switch (item.getItemId()){
//                    case R.id.navigation_home:
//                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
//                        finish();
//                        overridePendingTransition(0,0);
//                        return;
//                    case R.id.navigation_categories:
//
//                    case R.id.navigation_cart:
//                        startActivity(new Intent(getApplicationContext(),MyCartActivity.class));
//                        finish();
//                        overridePendingTransition(0,0);
//                        return;
//                    case R.id.navigation_person:
//                        startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
//                        finish();
//                        overridePendingTransition(0,0);
//                        return;
//
//                }
//            }
//        });


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;
                case R.id.navigation_categories:

                case R.id.navigation_cart:
                    startActivity(new Intent(getApplicationContext(),MyCartActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;
                case R.id.navigation_person:
                    startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
                    finish();
                    overridePendingTransition(0,0);
                    return true;

            }
            return true;

        }
    };
    private void category() {
        categoryList.clear();
        recyclerView_category.setVisibility(View.VISIBLE);
        StringRequest request=new StringRequest(Request.Method.POST, Keys.URL.CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                swipeRefreshLayout.setRefreshing(false);
                recyclerView_category.setVisibility(View.GONE);
                Loggers.i(response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        ArrayList<CategoryList> allCategoryList = new ArrayList<CategoryList>();
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++)
                        {
                            JSONObject jo=jsonArray.getJSONObject(i);
                            if(jo.getString("product").equals("0")){
                                JSONArray jAp = jo.getJSONArray("products");
                                ArrayList<ProductNameList> alproductName = new ArrayList<ProductNameList>();
                                for(int j=0; j<jAp.length(); j++){
                                    JSONObject json = jAp.getJSONObject(j);
                                    String p_name=json.getString("p_name");
                                    ProductNameList productNameList = new ProductNameList(json.getString("p_name"));
                                    alproductName.add(productNameList);
                                }
                                CategoryList allCategory = new CategoryList(jo.getString("c_id"),
                                        jo.getString("c_name"), jo.getString("c_photo"),jo.getString("c_enable"),jo.getString("MAX(product.p_discount)"),alproductName);
                                allCategoryList.add(allCategory);
                            }

//                            categoryList.add(new CategoryList(
//                                    jsonObject1.getString("c_id"),
//                                    jsonObject1.getString("c_name"),
//                                    jsonObject1.getString("c_photo"),
//                                    jsonObject1.getString("c_enable"),
//                                    jsonObject1.getString("MAX(product.p_discount)")));
                        }

                        recyclerView_category.setVisibility(View.VISIBLE);
                        adapter=new CategoryListAdapter(CategoriesActivity.this,allCategoryList);
                        recyclerView_category.setAdapter(adapter);
                        shimmerFrameLayout.setVisibility(View.GONE);


                    }else {
                        Toast.makeText(CategoriesActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(CategoriesActivity.this, "Technical problem arises", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                recyclerView_category.setVisibility(View.GONE);
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                return params;
            }
        };
        AppController.getInstance().add(request);


    }
    private void unregistertoken(final String token) {
        StringRequest request=new StringRequest(Request.Method.POST,Keys.URL.UNREGISTER_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("vic","UnRegisterToken=>"+response);
                try{

                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.getString("success").equals("1")){
                        Intent intent=new Intent(CategoriesActivity.this,LoginActivity.class);
                        GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(getApplicationContext(),gso);
                        googleSignInClient.signOut();
                        SharedPreference.removeKey("u_id");
                        SharedPreference.removeKey("f_token");
                        startActivity(intent);
                        finish();


                    }else {

                    }
                }catch (JSONException e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("f_token",token);
                return params;
            }
        };
        AppController.getInstance().add(request);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        finish();
    }
}