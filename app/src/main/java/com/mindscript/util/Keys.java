package com.mindscript.util;

public class Keys {
    public static String HOME_PATH="http://shopyourneeds.xyz/";
    public static String CATEGORY_PATH=HOME_PATH+"admin/uploads/category/";
    public static String PRODUCT_PATH=HOME_PATH+"admin/uploads/product/";
    public static String IMAGE_PATH=HOME_PATH+"admin/uploads/banner/";
    public static String NOTIFICATION_IMAGE_PATH=HOME_PATH+"admin/uploads/notificationphoto/";

    public static String VOLUNTEER_PATH=HOME_PATH+"admin/uploads/volunteer/";

    public static class URL{
        public static String LOGIN=HOME_PATH+"api/login.php";
        public static String REGISTER=HOME_PATH+"api/u_register.php";
        public static String PROFILE_REGISTER=HOME_PATH+"api/profileregister.php";
        public static String CATEGORY_LIST=HOME_PATH+"api/viewallcategories.php";
        public static String PRODUCT_LIST=HOME_PATH+"api/viewproducts.php";
        public static String VIEW_BANNERS=HOME_PATH+"api/viewbanners.php";
        public static String VIEW_PROFILE=HOME_PATH+"api/viewprofile.php";
        public static String SUBMIT_ORDER=HOME_PATH+"api/submit_order.php";
        public static String MY_ORDERS = HOME_PATH + "api/get_my_orders.php";
        public static String ORDER_DETAILS = HOME_PATH + "api/get_order_details.php";
        public static String REGISTER_TOKEN = HOME_PATH + "api/register_token.php";
        public static String UNREGISTER_TOKEN = HOME_PATH + "api/unregister_token.php";
        public static String ALL_PRODUCT_LIST=HOME_PATH+"api/viewallproducts.php";
        public static String VIEW_SERVICES=HOME_PATH+"api/viewservices.php";
        public static String SERVICE_ENQUIRY=HOME_PATH+"api/addserviceenquiry.php";
        public static String CONTACT_ENQUIRY=HOME_PATH+"api/addcontactenquiry.php";
        public static String MY_SERVICE_ENQUIRY=HOME_PATH+"api/get_my_service_enquiry.php";
        public static String FORGOT_PASSWORD=HOME_PATH+"api/forgot_password.php";
        public static String CHANGE_PASSWORD=HOME_PATH+"api/change_password.php";
        public static String VERSION = HOME_PATH + "api/version.php";
        public static String SUGGESTION_PRODUCT = HOME_PATH + "api/suggestionapi.php";
        public static String SUGGESTION_PRODUCT_DETAILS=HOME_PATH+"api/suggestionproductdetails.php";
        public static String CHECK_EMAIL=HOME_PATH+"api/checkemail.php";
        public static String VOLUNTEER_DETAILS=HOME_PATH+"api/volunteerdetails.php";

    }

    public static class TABLE{
        public static String DB_NAME="shopyourneed.db";
        public static int DB_VERSION=1;

    }
    public static class CARTTABLE{
        public static String TB_NAME="productcart";
    }

    public static class CART{
        public static String PC_ID="pc_id";
        public static String U_ID="u_id";
        public static String P_ID = "p_id";
        public static String C_ID = "c_id";
        public static String P_NAME="p_name";
        public static String P_PRICE="p_price";
        public static String P_DIS_TYPE="p_dis_type";
        public static String P_DISCOUNT="p_discount";
        public static String P_QUANTITY="p_quantity";
        public static String UN_ID="un_id";
        public static String P_PHOTO="p_photo";
        public static String UN_NAME="un_name";
        public static String U_QUANTITY="u_quantity";



    }
    public static String VERSION = "1.2";


}
