package com.mindscript.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CartModel {
    public static DbHelper dbHelper;
    public static SQLiteDatabase database;

    public static DbHelper getInstance(Context context) {

        if (dbHelper == null)
            dbHelper = new DbHelper(context);
        return dbHelper;
    }

    public static void open() {

        database = dbHelper.getWritableDatabase();
    }

    public static void close() {

        database.close();
    }



    public static void insert(String u_id,String p_id, String c_id, String p_name,String p_price,String p_dis_type,String p_discount,String p_quantity,String un_id,String p_photo,String un_name,String u_quantity) {
        ContentValues cv = new ContentValues();
        cv.put(Keys.CART.U_ID, u_id);
        cv.put(Keys.CART.P_ID, p_id);
        cv.put(Keys.CART.C_ID, c_id);
        cv.put(Keys.CART.P_NAME, p_name);
        cv.put(Keys.CART.P_PRICE, p_price);
        cv.put(Keys.CART.P_DIS_TYPE,p_dis_type);
        cv.put(Keys.CART.P_DISCOUNT,p_discount);
        cv.put(Keys.CART.P_QUANTITY, p_quantity);
        cv.put(Keys.CART.UN_ID, un_id);
        cv.put(Keys.CART.P_PHOTO,p_photo);
        cv.put(Keys.CART.UN_NAME, un_name);
        cv.put(Keys.CART.U_QUANTITY, u_quantity);

        database.insert(Keys.CARTTABLE.TB_NAME, null, cv);
        Log.i("NIK", "Value inserted");

    }

}
