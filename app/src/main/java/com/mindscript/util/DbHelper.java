package com.mindscript.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    String create_cart_table = "create table " + Keys.CARTTABLE.TB_NAME + "("
            + Keys.CART.PC_ID + " integer primary key autoincrement, "
            + Keys.CART.U_ID + " text not null,"
            + Keys.CART.P_ID + " text not null,"
            + Keys.CART.C_ID + " text not null,"
            + Keys.CART.P_NAME + " text not null,"
            + Keys.CART.P_PRICE + " text not null,"
            + Keys.CART.P_DIS_TYPE + " text not null,"
            + Keys.CART.P_DISCOUNT + " text not null,"
            + Keys.CART.P_QUANTITY + " text not null,"
            + Keys.CART.UN_ID + " text not null,"
            + Keys.CART.P_PHOTO + " text not null,"
            + Keys.CART.UN_NAME + " text not null,"
            + Keys.CART.U_QUANTITY + " text not null )";


    public DbHelper(@Nullable Context context) {
        super(context, Keys.TABLE.DB_NAME, null, Keys.TABLE.DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(create_cart_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
