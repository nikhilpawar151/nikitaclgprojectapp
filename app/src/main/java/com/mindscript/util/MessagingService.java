package com.mindscript.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mindscript.shopyouneeds.OrderDetailsActivity;
import com.mindscript.shopyouneeds.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Random;

public class MessagingService extends FirebaseMessagingService {
    long when = System.currentTimeMillis();
    NotificationCompat.Builder notificationBuilder;
    Intent i;
    Bitmap image;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.i("nik","Notify => " + remoteMessage.getData().toString());
        ShowNotification(remoteMessage.getData());



    }
    private void ShowNotification(Map<String, String> data) {


        if (data.containsKey("title")){
            Log.i("nik", String.valueOf(data.get("title")));
        }

        i = new Intent(MessagingService.this, OrderDetailsActivity.class);
        i.putExtra("o_id", data.get("o_id"));
        i.putExtra("o_no", data.get("o_no"));
        i.putExtra("o_time", data.get("o_time"));
        i.putExtra("o_total",data.get("o_total"));
        i.putExtra("o_status", data.get("o_status"));
        i.putExtra("type", "place_order");
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Log.i("nik", String.valueOf(data));


//        i = new Intent(MessagingService.this, OrderDetailsActivity.class);
//        i.putExtra("o_id","1");
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final int randomCode = new Random().nextInt(50) + 1;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, randomCode /* Request code */, i,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = this.getString(R.string.default_notification_channel_id);
        URL url = null;
        try {
            url = new URL(Keys.NOTIFICATION_IMAGE_PATH + data.get("image"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        notificationBuilder =
                new NotificationCompat.Builder(MessagingService.this, channelId)
                        .setContentTitle(data.get("title"))
                        .setContentText(data.get("message"))
                        .setContentInfo("message")
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
//                         .setStyle(new NotificationCompat.BigPictureStyle()
//                         .bigPicture(image).bigLargeIcon(null))
                        .setWhen(when)
                        .setContentIntent(pendingIntent);
        final NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Shop Your Needs",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        //  notificationBuilder.setContentTitle(data.get(Keys.News.N_TITLE))
        //       .setContentText(data.get(Keys.News.N_DESCRIPTION));
        notificationManager.notify(randomCode, notificationBuilder.build());


    }

}
